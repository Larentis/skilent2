/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Role = mongoose.model('Role'),
    config = require('../../config/config'),
    _ = require('underscore');

/**
 * Create role
 */
exports.create = function(req, res) {
    var role = new Role(req.body);
    role.updatedBy = req.user.id;
    //console.log(req.body);

    role.save(function(err) {
        if (!err) {
            console.log("Role saved to file.");
            res.redirect('/api/v1/roles/' + role._id);

        } else {
            console.log("Role not saved. " + err);
        }
    });


};

/**
 * Update a role
 */
exports.update = function(req, res) {
    var role = req.profile;

    // clone object using underscore util
    role = _.extend(role, req.body);

    role.save(function(err) {
        if (!err) {
            console.log("Role " + req.profile._id + " updated on file.");
            return res.json(req.profile || null);

        } else {
            console.log("Role not updated. " + err);
        }
    });
};

/**
 *
 * Send a specific role
 */
exports.showRole = function(req, res) {
    res.json(req.profile || null);
    console.log('Here is the role you want !');
    //console.log(req);
    //console.log(req.profile);
};

/**
 * Delete a role
 */
exports.destroy = function(req, res) {
    var role = req.profile;

    role.remove(function(err) {
        if (err) {
            console.log("My error: " + err);
            res.send(500, { errors: err });
        } else {
            res.jsonp(role);
            res.redirect('/api/v1/roles/' + role._id);
        }
    });
};


/**
 * Find role by id
 */
exports.role = function(req, res, next, id) {
    Role
        .findOne({
            _id: id
        })
        .exec(function(err, role) {
            if (err) return next(err);
            if (!role) return next(new Error('Failed to load role ' + id));
            req.profile = role;
            next();
        });
};


/**
 * List of Roles
 */
exports.all = function(req, res) {
    Role.find().sort('-name').select('name applications candidates jobs roles users applicationind candidateind jobind roleind userind updatedBy created').exec(function(err, roles) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(roles);
        }
    });
};

/**
 * Find role by name
 */
function roleNames() {
    Role
        .find()
        .exec(function(err, roles) {
            if (err) return next(err);
            if (!roles){
                roles = null;
            }
            r = roles;
            return r;
        });
    return r;
}


/**
 * Access control middleware
 */
exports.isPermitted = function (req, res, next) {

    v = false;
    b = req.user.roles;
    try { c = roleNames(); }
    catch(e){
        return next();
        //return res.send(403, 'User privileges indeterminate. Please contact the Systems Administrator for more information.');
    }
    d = new Array(c.length);
    a = new Array(c.length);
    e = new Array(req.user.roles.length);

    switch (req.route.method) {
        case 'get':
            x = 'read';
            break;
        case 'post':
            x = 'create';
            break;
        case 'put':
            x = 'update';
            break;
        case 'delete':
            x = 'delete';
            break;
    }
    switch (req.route.path) {
        case '/api/v1/applications':
            y = 'applications';
            break;
        case '/api/v1/applications/:applicationId':
            y = 'applicationind';
            break;
        case '/api/v1/candidates':
            y = 'candidates';
            break;
        case '/api/v1/candidates/:candidateId':
            y = 'candidateind';
            break;
        case '/api/v1/jobs':
            y = 'jobs';
            break;
        case '/api/v1/jobs/:jobId':
            y = 'jobind';
            break;
        case '/api/v1/roles':
            y = 'roles';
            break;
        case '/api/v1/roles/:roleId' :
            y = 'roleind';
            break;
        case '/users':
            y = 'users';
            break;
        case '/api/v1/users':
            y = 'users';
            break;
        case '/users/:userId':
            y = 'userind';
            break;
        case '/api/v1/users/:userId':
            y = 'userind';
            break;
    }

    for (w = 0; w < c.length; w++) {
        d[w] = c[w];
        for (q = 0; q < req.user.roles.length; q++) {
            if (b[q] == d[w].name) {
                e[q] = d[w][y][x];

            }
        }
    }

    for (g = 0; g < e.length; g++) {
        if (e[g] === true) {
            v = true;
        }
    }

    if (v == false){
        console.log('Denied');
        return res.send(403, 'User is not authorized. Contact System Administrator for information regarding user privileges.');
    }


    v = null;
    a = null;
    b = null;
    c = null;
    d = null;
    e = null;
    next();
};
