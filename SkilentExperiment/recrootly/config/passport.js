var mongoose = require('mongoose'),
    LocalStrategy = require('passport-local').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    GitHubStrategy = require('passport-github').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    LinkedinStrategy = require('passport-linkedin').Strategy,
    ApplyViaLinkedinStrategy = require('passport-applywithlinkedin').Strategy,
    BasicStrategy = require('passport-http').BasicStrategy,
    ObjectId = require('mongoose').Types.ObjectId,
    User = mongoose.model('User'),
    Candidate = mongoose.model('Candidate'),
    Job = mongoose.model('Job'),
    JApp = mongoose.model('Application'),
    util = require('util'),
    _ = require('underscore'),
    config = require('./config'),
    mailer = require('../app/controllers/mailer'),
    appln = require('../app/controllers/applications');


module.exports = function(passport) {
    //Serialize sessions
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findOne({
            _id: id
        }, '-salt -hashed_password', function(err, user) {
            done(err, user);
        });
    });


    // Use the HTTP BasicStrategy within Passport for API authenticaion over HTTPS.
    passport.use(new BasicStrategy(
        function(username, password, done) {
            // Find the user by email.  If there is no user with the given
            // email address, or the password is not correct, set the user to `false` to
            // indicate failure.  Otherwise, return the authenticated `user`            
            User.findOne({
                email: username
            }, function(err, user, info) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown user'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'Invalid password'
                    });
                }
                return done(null, user, info);
            });
        }
    ));

    //Use local strategy
    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function(email, password, done) {
            User.findOne({
                email: email
            }, function(err, user, info) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Unknown user'
                    });
                }
                if (!user.authenticate(password)) {
                    return done(null, false, {
                        message: 'Invalid password'
                    });
                }
                return done(null, user, info);
            });
        }
    ));

    // use linkedin strategy
    passport.use(new LinkedinStrategy({
            consumerKey: config.linkedin.clientID,
            consumerSecret: config.linkedin.clientSecret,
            callbackURL: config.linkedin.callbackURL,
            profileFields: ['id', 'first-name', 'last-name', 'email-address'],
            passReqToCallback: true
        },
        function(req, token, tokenSecret, profile, done) {
            console.log('this is the route for a sign-in' + req.body);
            User.findOne({
                'linkedin.id': profile.id 
            }, function (err, user, info) {
                if (!user) {
                    user = new User({
                        name: 
                        {
                            first: profile.name.givenName,
                            last: profile.name.familyName
                        },
                        email: profile.emails[0].value,
                        regStatus: "New",
                        //username: profile.emails[0].value,
                        provider: 'linkedin',
                        linkedin: profile._json
                  });lo
                  user.save(function (err) {
                    if (err) console.log(err);

                    // Notify Admin that User has registered
                    var locals = {
                       email: config.mailer.defaultFromAddress,
                       subject: 'User Registered',
                       name: {
                        first: user.name.first,
                        last: user.name.last
                       },
                       userEmail: user.email,
                       provider : user.provider,
                       activateUrl: config.host + '#!/admin123/'
                    };

                   mailer.sendOne('registration', locals, function (myerr, responseStatus, html, text) {
                        if (myerr) {
                            console.log("Error sending email to admin: " + err);
                            return done(myerr, user, info);
                        } else {
                            console.log("User registration email sent to admin for userid: " + user._id);
                        }
                    });

                    return done(err, user);
                  });
                } else {
                    req.session.token = {token:token, secret:tokenSecret};
                    return done(err, user, info)
                }
            });
        }
    ));

    // use Apply with Linkedin strategy
    passport.use(new ApplyViaLinkedinStrategy({
            consumerKey: config.applywithlinkedin.clientID,
            consumerSecret: config.applywithlinkedin.clientSecret,
            callbackURL: config.applywithlinkedin.callbackURL,
            profileFields: ['id', 'first-name', 'last-name', 'email-address', 'headline', 'educations', 'languages', 'summary', 'skills','three-current-positions', 'certifications', 'positions', 'location', 'recommendations-received', 'public-profile-url'],
            passReqToCallback: true
        },
        function(req, token, tokenSecret, profile, done) {


            var jApp = new JApp();
            var application = profile;
            application.job = {id : req.session.passport.jobId};
            application.phone = req.session.passport.phone;


            application.job.id = new ObjectId(application.job.id);
            console.log(application);

            if (!(application === undefined)) {
                Candidate.findOne({
                    email: application._json.emailAddress
                })
                    .populate('jobsAppliedTo', 'job')
                    .exec(function(err, user) {
                        if (err) return done(err);
                        if (!user) {
                            ///////////////////////////////////////////////////////////////////////////////////////////
                            // Candidate does not exists, create candidate, create application, associate cand to app
                            ///////////////////////////////////////////////////////////////////////////////////////////


                            exp = JSON.parse(profile._raw);
                            console.log(exp);

                            if (exp.threeCurrentPositions && exp.threeCurrentPositions.values) {
                                var n = exp.threeCurrentPositions.values[0].title;
                            }

                            if (exp.threeCurrentPositions == undefined){
                                n = null;
                            }


                        if (!cand) {
                            var cand = new Candidate({
                                name: {
                                    first: profile.name.givenName,
                                    last: profile.name.familyName
                                },
                                email: profile.emails[0].value,
                                provider: 'linkedin',
                                linkedin: profile._raw,
                                title: n,
                                description: profile._json.headline,
                                phone: application.phone,
                                linkedinProfile: exp.publicProfileUrl,
                                jobsAppliedTo: req.session.passport.jobId
                            });

                            // Linkedin Educations
                            if (exp.educations && exp.educations.values) {
                                var ed = exp.educations.values;
                                cand.educations = [];
                                for (var n=0; n < exp.educations.values.length; n++) {
                                    var myed = {};
                                    myed.fieldOfStudy = ed[n].fieldOfStudy;
                                    myed.degree = ed[n].degree;
                                    myed.schoolName = ed[n].schoolName;
                                    myed.activities = ed[n].activities;
                                    myed.startDate = {};
                                    if (ed[n].startDate) {
                                        if (ed[n].startDate.month) {
                                            myed.startDate.month = ed[n].startDate.month;
                                        }
                                        if (ed[n].startDate.year) {
                                            myed.startDate.year = ed[n].startDate.year;
                                        }
                                    }
                                    myed.endDate = {};
                                    if (ed[n].endDate) {
                                        if (ed[n].endDate.month) {
                                            myed.endDate.month = ed[n].endDate.month;
                                        }
                                        if (ed[n].endDate.year) {
                                            myed.endDate.year = ed[n].endDate.year;
                                        }
                                    }
                                    cand.educations.push(myed);
                                }
                            }

                            // Linkedin Certifications
                            if (exp.certifications && exp.certifications.values) {
                                cand.certifications = [];
                                var certs = exp.certifications.values;
                                for (var m=0; m < exp.certifications.values.length; m++) {
                                    cand.certifications.push(certs[m].name);
                                }
                            }

                            // Linkedin Languages
                            if (exp.languages && exp.languages.values){
                                // reset languages array before pushing to it
                                cand.languages = [];
                                var langs = exp.languages.values;
                                for (var l=0; l < exp.languages.values.length; l++) {
                                    cand.languages.push(langs[l].language.name);
                                }
                            }

                            // Linkedin Positions
                            if (exp.positions && exp.positions.values) {
                                var pos = exp.positions.values;
                                // reset value of positions array before pushing to it
                                cand.positions = [];
                                for (var j = 0; j < exp.positions.values.length; j++) {
                                    var mypos = {};
                                    mypos.title = pos[j].title;
                                    mypos.summary = pos[j].summary;
                                    mypos.startDate = {};
                                    if (pos[j].startDate) {
                                        if (pos[j].startDate.month) {
                                            mypos.startDate.month = pos[j].startDate.month;
                                        }
                                        if (pos[j].startDate.year) {
                                            mypos.startDate.year = pos[j].startDate.year;
                                        }
                                    }
                                    mypos.endDate = {};
                                    if (pos[j].endDate) {
                                        if (pos[j].endDate.month) {
                                            mypos.endDate.month = pos[j].endDate.month;
                                        }
                                        if (pos[j].endDate.year) {
                                            mypos.endDate.year = pos[j].endDate.year;
                                        }
                                    }
                                    mypos.isCurrent = pos[j].isCurrent;
                                    mypos.company = pos[j].company.name;
                                    cand.positions.push(mypos);
                                }
                            }

                            // Linkedin Recommendations
                            if (exp.recommendationsReceived && exp.recommendationsReceived.values) {
                                var recs = exp.recommendationsReceived.values;
                                cand.recommendations = [];
                                for (var i = 0; i < exp.recommendationsReceived.values.length; i++) {
                                    var myrec = {};
                                    myrec.recType = recs[i].recommendationType.code;
                                    myrec.recText = recs[i].recommendationText;
                                    myrec.recommender = recs[i].recommender.firstName + " " + recs[i].recommender.lastName;
                                    cand.recommendations.push(myrec);
                                }
                            }

                            // Linkedin Skills
                            if (exp.skills && exp.skills.values) {
                                var skills = exp.skills.values;
                                // reset the value of skills array before pushing
                                cand.skills = [];
                                for (var k=0; k < exp.skills.values.length; k++){
                                    cand.skills.push(skills[k].skill.name);
                                }
                            }

                            if (exp.location) {
                                cand.location = {};
                                cand.location.city = exp.location.name;
                                if (exp.location.country) {
                                    if (exp.location.country.code == 'us') {
                                        cand.location.market = 'U.S.';
                                    } else {
                                        cand.location.market = exp.location.country.code;
                                    }
                                } else {
                                    if (!cand.location.market) {
                                        cand.location.market = 'U.S.';
                                    }
                                }

                            }

                            // candidate creation
                            console.log(cand);
                            cand.save(function (candErr) {
                                if (candErr) {
                                    return done(candErr);
                                } else {
                                    jApp.candidate = cand._id;
                                    jApp.candidateName = application._json.firstName + " " + application._json.lastName;
                                    jApp.job = application.job.id;
                                    jApp.jobTitle = req.session.passport.jobTitle;
                                    // Parse EEO meta data
                                    if (req.session.passport.meta) {
                                        var eeoMeta = req.session.passport.meta.split(',');
                                        if (eeoMeta.length != 4) return done(400, {errors: 'Failed to include all 3 EEO meta data fields'});
                                        jApp.eeo.gender = eeoMeta[0];
                                        jApp.eeo.race = eeoMeta[1];
                                        jApp.eeo.veteran = eeoMeta[2];
                                        jApp.jobSource = eeoMeta[3];
                                        jApp.appSource = "LinkedIn";
                                    }
                                    // create job application
                                    jApp.save(function (err2) {
                                        if (err2) {
                                            return done(err2);
                                        } else {
                                            // update candidate with job application id
                                            cand.jobsAppliedTo.push(jApp._id);
                                            cand.save(function (err3) {
                                                if (err3) {
                                                    return done(err3);
                                                } else {
                                                    // update job with applicationid
                                                    Job.findByIdAndUpdate(req.session.passport.jobId, {$push: {jobApplications: jApp._id}}, function (err4, myjob) {
                                                        if (err4) return done(err4);
                                                        else {
                                                            done();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }} else {
                            /////////////////////////////////////////////////////////////////////////////////////
                            // Candidate already exists in the database, create application and update candidate
                            /////////////////////////////////////////////////////////////////////////////////////
                            var jobsApplied = _.pluck(user.jobsAppliedTo, 'job');
                            if (_.findWhere(jobsApplied, req.session.passport.jobId)) {
                                var errorString = "Candidate: " + user._id + " already applied to job: " + req.session.passport.jobId;
                                console.log(errorString);
                                return done(errorString);
                            } else {
                                console.log("Creating job application and updating linkedin candidate");
                                jApp.candidate = user._id;
                                jApp.candidateName = user.name.first + " " + user.name.last;
                                jApp.job = req.session.passport.jobId;
                                jApp.jobTitle = req.session.passport.jobTitle;
                                // Parse EEO meta data
                                if (req.session.passport.meta) {
                                    var eeoMeta = req.session.passport.meta.split(',');
                                    if (eeoMeta.length != 4) return res.send(400, {errors: 'Failed to include all 3 EEO meta data fields'});
                                    jApp.eeo.gender = eeoMeta[0];
                                    jApp.eeo.race = eeoMeta[1];
                                    jApp.eeo.veteran = eeoMeta[2];
                                    jApp.jobSource = eeoMeta[3];
                                    jApp.appSource = 'LinkedIn';
                                }
                                jApp.save(function(jappErr) {
                                    if (jappErr) {
                                        return done(jappErr);
                                    } else {
                                        // update candidate with linkedin profile
                                        var updateCand = appln.linkedinToCand(user, application);
                                        // update candidate with job application id
                                        updateCand.jobsAppliedTo.push(jApp._id);
                                        updateCand.save(function(err2) {
                                            if (err2) {
                                                return done(err2)
                                            } else {
                                                // update job with applicationid
                                                Job.findByIdAndUpdate(req.session.passport.jobId, { $push: { jobApplications: jApp._id }}, function (err3, myjob) {
                                                    if (err3) return res.send(400, {errors: err3});
                                                    else {
                                                        done();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
            }else{
                req.session.token = {token:token, secret:tokenSecret};
                return done();
        }
        }
    ));


    //Use twitter strategy
    passport.use(new TwitterStrategy({
            consumerKey: config.twitter.clientID,
            consumerSecret: config.twitter.clientSecret,
            callbackURL: config.twitter.callbackURL
        },
        function(token, tokenSecret, profile, done) {
            User.findOne({
                'twitter.id_str': profile.id
            }, function(err, user, info) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    var nameParts = profile.displayName.split(" "),
                        firstName = nameParts.shift(),
                        lastName = nameParts.shift() || "";                    
                    user = new User({
                        name: {
                            first: firstName,
                            last: lastName
                        },
                        regStatus: "New",
                        //username: profile.username,
                        email: util.format('%s@%s.twitter.id',
                                            profile.username,
                                            profile.id),
                        //email: profile.emails[0].value,
                        provider: 'twitter',
                        twitter: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user, info);
                }
            });
        }
    ));

    //Use facebook strategy
    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'facebook.id': profile.id
            }, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    var nameParts = profile.displayName.split(" "),
                        firstName = nameParts.shift(),
                        lastName = nameParts.shift() || "";                    
                    user = new User({
                        name: {
                            first: firstName,
                            last: lastName
                        },
                        email: profile.emails[0].value,
                        //username: profile.username,
                        provider: 'facebook',
                        facebook: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user);
                }
            });
        }
    ));

    //Use github strategy
    passport.use(new GitHubStrategy({
            clientID: config.github.clientID,
            clientSecret: config.github.clientSecret,
            callbackURL: config.github.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'github.id': profile.id
            }, function(err, user) {
                if (!user) {
                    var nameParts = profile.displayName.split(" "),
                        firstName = nameParts.shift(),
                        lastName = nameParts.shift() || "";                    
                    user = new User({
                        name: {
                            first: firstName,
                            last: lastName
                        },
                        email: profile.emails[0].value,
                        //username: profile.username,
                        provider: 'github',
                        github: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user);
                }
            });
        }
    ));

    //Use google strategy
    passport.use(new GoogleStrategy({
            clientID: config.google.clientID,
            clientSecret: config.google.clientSecret,
            callbackURL: config.google.callbackURL
        },
        function(accessToken, refreshToken, profile, done) {
            User.findOne({
                'google.id': profile.id
            }, function(err, user, info) {
                if (!user) {
                        var nameParts = profile.displayName.split(" "),
                            firstName = nameParts.shift(),
                            lastName = nameParts.shift() || "";
                    user = new User({
                        name: {
                            first: firstName,
                            last: lastName
                        },
                        email: profile.emails[0].value,
                        regStatus: "New",
                        //username: profile.username,
                        provider: 'google',
                        google: profile._json
                    });
                    user.save(function(err) {
                        if (err) console.log(err);
                        return done(err, user);
                    });
                } else {
                    return done(err, user, info);
                }
            });
        }
    ));
};