//Job Application factory service used for REST endpoint
angular.module('mean.applications').factory('Applications', ['$http', '$q', function($http, $q) {

	var urlBase = '/api/v1/applications';
	var Applications = {};

	Applications.getAll = function() {
		return $http.get(urlBase);
	};

	Applications.getApp = function(appId, jobId) {
		return $http.get(urlBase + '/' + appId);
	};

	Applications.updateApp = function(app) {
		return $http.put(urlBase + '/' + app._id, app);
	};

	return Applications;

}]);