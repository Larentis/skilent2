/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    Schema = mongoose.Schema;


/**
+ * Job Application Schema
+ */
var ApplicationSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    updated : { type: Date, default: Date.now, required: true },
    updatedBy: {
        type: Schema.ObjectId,
        ref: 'User'
    },        
    survey: {
        created: {
            type: Date,
            default: Date.now
        },  
        techincal: Number,
        communications: Number,
        leadership: Number,
        social: Number,
        comments: String, 
        createdBy: { type: String }
    },  
    candidate: {
        type: Schema.ObjectId,
        ref: 'Candidate',
        required: true
    },
    candidateName: {type: String},
    jobSource: {type: String},
    appSource: {type: String, required: true},
    job: {
        type: Schema.ObjectId,
        ref: 'Job',
        required: true
    },
    jobTitle: {type: String},
    status: { type : String, default: 'Filed' },
    eeo: {
        created: {
            type: Date,
            default: Date.now
        },  
        updated: {
            type: Date,
            default: Date.now
        },          
        gender: { type: String },
        race: { type: String },
        veteran: { type: String }        
    }
});
// including candidate name in this model to eliminate a query

/**
 * Statics
 */
ApplicationSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('candidate', '_id name email').populate('updatedBy', 'name email')
        .populate('job', '_id projName title').exec(cb);
};

mongoose.model('Application', ApplicationSchema);