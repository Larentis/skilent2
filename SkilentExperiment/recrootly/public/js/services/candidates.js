//Candidates service used for candidates REST endpoint
// angular.module('mean.candidates').factory("Candidates", ['$resource', function($resource) {
//     return $resource('/api/v1/candidates/:candidateId', {
//         candidateId: '@_id'
//     }, {
//         update: {
//             method: 'PUT'
//         }
//     });
// }]);


//Candidate factory service used for REST endpoint
angular.module('mean.candidates').factory('Candidates', ['$http', '$q', function($http, $q) {

	var urlBase = '/api/v1/candidates';
	var Candidates = {};

	Candidates.getCand = function(id) {
		return $http.get(urlBase + '/' + id);
	};

	Candidates.getAll = function() {
		return $http.get(urlBase);
	};

	Candidates.insertCand = function(cand, file) {
		var fd = new FormData();
		fd.append("candidate", angular.toJson(cand));
		console.log(angular.toJson(cand));
		console.log(cand);
		fd.append('file', file);

		return $http.post(urlBase, fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		});
	};

	Candidates.updateCand = function(id, cand) {
		return $http.put(urlBase + '/' + id, cand);
	};

	Candidates.remove = function(cand) {
		return $http.delete(urlBase + '/' + cand._id, cand);
	};

	Candidates.signin = function(formData) {
		return $http.post(urlBase + '/session', formData);
	};


	return Candidates;

}]);