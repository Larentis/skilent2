angular.module('mean.jobs').controller('JobsController', ['$scope', '$routeParams', '$location', 'Global', 'JobsSrv', 'UserServices', '_', '$window', function ($scope, $routeParams, $location, Global, JobsSrv, UserServices, _, $window) {
    $scope.global = Global;
    $scope.skills = [];

    $scope.create = function() {

        // var candidate = new Candidates({
        //     name: {
        //         first: this.name.first,
        //         last: this.name.last
        //     }, 
        //     title: this.title,
        //     salary: {
        //         current: this.salary.current,
        //         desired: this.salary.desired
        //     },
        //     description: this.description,
        //     resume: this.resume
        // });

        $scope.job.skills = $scope.skills;


        JobsSrv.insertJob($scope.job).success(function(response) {
           console.log("My new job id: " + response._id);
           $location.path("jobs/" + response._id);
        })
        .error(function(err, status) {
            $window.scrollTo(0, 0);
            $scope.errorMessage = status + ": Error inserting job. " + JSON.stringify(err.errors);
        });

    };

    $scope.showAddJobButton = function() {
        //if(){
          //  return true;
       // }
         return true;

    };


    $scope.update = function() {
        var job = $scope.job;
        $scope.errorMessage = '';

        JobsSrv.updateJob(job).success(function(response) {
            console.log("Updated job ID: " + response._id);
            $location.path('jobs/' + response._id);
        })
        .error(function(err, status) {
            $window.scrollTo(0, 0);
            $scope.errorMessage = "Error updating job. " + JSON.stringify(err.errors);

        });
    };

    $scope.find = function() {
        JobsSrv.getAll().success(function(data, status) {
            $scope.jobs = data;
        })
        .error(function(err, status) {
            $scope.errorMessage = "Jobs list not retrieved. " + err;
        });
    };

    $scope.findOneEdit = function() {
        JobsSrv.getJob($routeParams.jobId).success(function(data, status) {
            $scope.job = data;
            $scope.job.salary.public = $scope.job.salary.public.toString();
            $scope.job.leadRec = $scope.job.leadRec._id;
        });
    };

    $scope.findOne = function() {
        if ($routeParams.viewApps)
        {
            $scope.isActive = true;
        }
        JobsSrv.getJob($routeParams.jobId).success(function(data, status) {
            $scope.job = data;
            $scope.job.salary.public = $scope.job.salary.public.toString();
        });
    };

    $scope.modalremove = function() {
        $scope.errorMessage = '';
        job = $scope.markForDeletion;
        if (job) {
            JobsSrv.remove(job).success(function(data, status) {  
                for (var i in $scope.jobs) {
                    if ($scope.jobs[i] == job) {
                        $scope.jobs.splice(i, 1);
                    }
                }
            })
            .error(function(err, status) {
                $scope.errorMessage = "Error removing job: " + status + ": " + err;
            });
        }
        //else {
        //    console.log("Removing single candidate, not from list");
        //    Candidates.remove($scope.job._id, $scope.job);
        //    $location.path('jobs');
        //}

    };    

    $scope.selectJob = function(job) {
        $scope.markForDeletion = job;
    }; 


    $scope.addSkill = function() {
        var partsOfStr = $scope.newSkill.name.split(',');
        for (var i in partsOfStr) {
            $scope.skills.push(partsOfStr[i]);
        }
        $scope.newSkill.name = '';
    };

    $scope.addSkillEditMode = function() {
        var partsOfStr = $scope.newSkill.name.split(',');
        for (var i in partsOfStr) {
            if (!$scope.job.skills) {
                $scope.job.skills = [];
            }
            $scope.job.skills.push(partsOfStr[i]);
        }
        $scope.newSkill.name = '';
    };

    $scope.removeSkill = function(skill) {
        var i = $scope.skills.indexOf(skill);
        if(i != -1) {
            $scope.skills.splice(i, 1);
        }
    };

    $scope.removeSkillEditMode = function(skill) {
        var i = $scope.job.skills.indexOf(skill);
        if(i != -1) {
            $scope.job.skills.splice(i, 1);
        }
    };

   UserServices.getUsers().then(function(data) {
       $scope.userList = data;
   });

//////////////////////////////////////////////////////////////
// Status Filter Buttons
//////////////////////////////////////////////////////////////
    $scope.selectedStatus = _.pluck($scope.comtechJobStatus, 'code');

    $scope.setSelectedStatus = function (code, evt) {
        console.log("My id: " + code);
        if (_.contains($scope.selectedStatus, code)) {
            $scope.selectedStatus = _.without($scope.selectedStatus, code);
        } else {
            $scope.selectedStatus.push(code);
        }
        evt.stopPropagation();
        return false;
    };

    $scope.isChecked = function (id) {
        if (_.contains($scope.selectedStatus, id)) {
            return 'glyphicon glyphicon-ok pull-right';
        }
        return false;
    };

    $scope.checkAll = function () {
        $scope.selectedStatus = _.pluck($scope.comtechJobStatus, 'code');
        //console.log($scope.selectedStatus);
    };

    $scope.filterByStatus = function(job) {
        return ($scope.selectedStatus.indexOf(job.status) !== -1);
    };


//////////////////////////////////////////////////////////////
// Company Filter Buttons
//////////////////////////////////////////////////////////////
    $scope.selectedCompany = _.pluck($scope.companies, 'code');

    $scope.setSelectedCompany = function (code, evt) {
        if (_.contains($scope.selectedCompany, code)) {
            $scope.selectedCompany = _.without($scope.selectedCompany, code);
        } else {
            $scope.selectedCompany.push(code);
        }
        evt.stopPropagation();
        return false;
    };

    $scope.isCompanyChecked = function (id) {
        if (_.contains($scope.selectedCompany, id)) {
            return 'glyphicon glyphicon-ok pull-right';
        }
        return false;
    };

    $scope.checkAllCompanies = function () {
        $scope.selectedCompany = _.pluck($scope.companies, 'code');
    };

    $scope.filterByCompany = function(job) {
        return ($scope.selectedCompany.indexOf(job.company) !== -1);
    };


//////////////////////////////////////////////////////////////
// Market Filter Buttons
//////////////////////////////////////////////////////////////
    $scope.selectedMarket = _.pluck($scope.comtechMarkets, 'code');

    $scope.setSelectedMarket = function (code, evt) {
        console.log("My market id: " + code);
        if (_.contains($scope.selectedMarket, code)) {
            $scope.selectedMarket = _.without($scope.selectedMarket, code);
        } else {
            $scope.selectedMarket.push(code);
        }
        evt.stopPropagation();
        return false;
    };

    $scope.isMarketChecked = function (id) {
        if (_.contains($scope.selectedMarket, id)) {
            return 'glyphicon glyphicon-ok pull-right';
        }
        return false;
    };

    $scope.checkAllMarkets = function () {
        $scope.selectedMarket = _.pluck($scope.comtechMarkets, 'code');
    };

    $scope.filterByMarket = function(job) {
        return ($scope.selectedMarket.indexOf(job.location.market) !== -1);
    };

//////////////////////////////////////////////////////////////
// Static Values -- To be Created as Service [in the future]
//////////////////////////////////////////////////////////////

    $scope.companies = [
    {
        "name" : "CISPL",
        "code" : "CISPL"
    },
    {
        "name" : "CISPL Global",
        "code" : "CISPL Global"
    },
    {
        "name" : "Comtech",
        "code" : "Comtech"
    }
    ];

    $scope.category = [
    {
        "name" : "Executive / Senior Level Officials and Managers",
        "code" : "Executive / Senior Level Officials and Managers"
    },
    {
        "name" : "First / Mid Level Officials and Managers",
        "code" : "First / Mid Level Officials and Managers"
    },
    {
        "name" : "Professionals",
        "code" : "Professionals"
    },
    {
        "name" : "Technicians",
        "code" : "Technicians"
    },
    {
        "name" : "Sales Workers",
        "code" : "Sales Workers"
    },
    {
        "name" : "Administrative Support Workers",
        "code" : "Administrative Support Workers"
    },
    {
        "name" : "Craft Workers",
        "code" : "Craft Workers"
    },
    {
        "name" : "Operatives",
        "code" : "Operatives"
    },
    {
        "name" : "Laborers and Helpers",
        "code" : "Laborers and Helpers"
    },
    {
        "name" : "Service Workers",
        "code" : "Service Workers"
    }
    ];

    $scope.comtechJobStatus = [
    {
        "name" : "Open - Funded",
        "code" : "Open - Funded"
    },
    {
        "name" : "Open - Proposal",
        "code" : "Open - Proposal"
    },
    {
        "name" : "On Hold",
        "code" : "On Hold"
    },
    {
        "name" : "Filled W2",
        "code" : "Filled W2"
    },
    {
        "name" : "Filled Non-W2",
        "code" : "Filled Non-W2"
    },
    {
        "name" : "Cancelled",
        "code" : "Cancelled"
    },
    {
        "name" : "Closed",
        "code" : "Closed"
    }             
    ];

    $scope.comtechWorkAuth = [
    {
        "name" : "U.S. Citizen",
        "code" : "U.S. Citizen"
    },
    {
        "name" : "Green Card holder",
        "code" : "Green Card holder"
    },
    {
        "name" : "Current H1B",
        "code" : "Current H1B"
    },
    {
        "name" : "Other",
        "code" : "Other"
    }
    ];

    $scope.comtechEducation = [
    {
        "name" : "High school diploma / GED",
        "code" : "High school diploma / GED"
    },
    {
        "name" : "Associate Degree",
        "code" : "Associate Degree"
    },
    {
        "name" : "Bachelor's Degree",
        "code" : "Bachelor's Degree"
    },
    {
        "name" : "Master's degree",
        "code" : "Master's degree"
    },
    {
        "name" : "Doctorate",
        "code" : "Doctorate"
    },
    {
        "name" : "Other",
        "code" : "Other"
    }
    ];

    $scope.comtechJobTypes = [
        "W2", "1099", "W2/1099"
    ]; 

    $scope.comtechPublicSalary = [
    {
        "name" : "Salary should be private",
        "code" : "false"
    },
    {
        "name" : "Salary should be public",
        "code" : "true"
    }
    ]; 

    $scope.comtechMarkets = [
    {
        "name" : "Europe",
        "code" : "Europe"
    },
    {
        "name" : "India",
        "code" : "India"
    },
    {
        "name" : "Southeast Asia",
        "code" : "Southeast Asia"
    },    
    {
        "name" : "U.S.",
        "code" : "U.S."
    }
    ];


    $scope.states = [
    {   
        "name" : "India",
        "abbreviation" : "India"
    },
    {
        "name": "Alabama",
        "abbreviation": "AL"
    },
    {
        "name": "Alaska",
        "abbreviation": "AK"
    },
    {
        "name": "American Samoa",
        "abbreviation": "AS"
    },
    {
        "name": "Arizona",
        "abbreviation": "AZ"
    },
    {
        "name": "Arkansas",
        "abbreviation": "AR"
    },
    {
        "name": "California",
        "abbreviation": "CA"
    },
    {
        "name": "Colorado",
        "abbreviation": "CO"
    },
    {
        "name": "Connecticut",
        "abbreviation": "CT"
    },
    {
        "name": "Delaware",
        "abbreviation": "DE"
    },
    {
        "name": "District Of Columbia",
        "abbreviation": "DC"
    },
    {
        "name": "Federated States Of Micronesia",
        "abbreviation": "FM"
    },
    {
        "name": "Florida",
        "abbreviation": "FL"
    },
    {
        "name": "Georgia",
        "abbreviation": "GA"
    },
    {
        "name": "Guam",
        "abbreviation": "GU"
    },
    {
        "name": "Hawaii",
        "abbreviation": "HI"
    },
    {
        "name": "Idaho",
        "abbreviation": "ID"
    },
    {
        "name": "Illinois",
        "abbreviation": "IL"
    },
    {
        "name": "Indiana",
        "abbreviation": "IN"
    },
    {
        "name": "Iowa",
        "abbreviation": "IA"
    },
    {
        "name": "Kansas",
        "abbreviation": "KS"
    },
    {
        "name": "Kentucky",
        "abbreviation": "KY"
    },
    {
        "name": "Louisiana",
        "abbreviation": "LA"
    },
    {
        "name": "Maine",
        "abbreviation": "ME"
    },
    {
        "name": "Marshall Islands",
        "abbreviation": "MH"
    },
    {
        "name": "Maryland",
        "abbreviation": "MD"
    },
    {
        "name": "Massachusetts",
        "abbreviation": "MA"
    },
    {
        "name": "Michigan",
        "abbreviation": "MI"
    },
    {
        "name": "Minnesota",
        "abbreviation": "MN"
    },
    {
        "name": "Mississippi",
        "abbreviation": "MS"
    },
    {
        "name": "Missouri",
        "abbreviation": "MO"
    },
    {
        "name": "Montana",
        "abbreviation": "MT"
    },
    {
        "name": "Nebraska",
        "abbreviation": "NE"
    },
    {
        "name": "Nevada",
        "abbreviation": "NV"
    },
    {
        "name": "New Hampshire",
        "abbreviation": "NH"
    },
    {
        "name": "New Jersey",
        "abbreviation": "NJ"
    },
    {
        "name": "New Mexico",
        "abbreviation": "NM"
    },
    {
        "name": "New York",
        "abbreviation": "NY"
    },
    {
        "name": "North Carolina",
        "abbreviation": "NC"
    },
    {
        "name": "North Dakota",
        "abbreviation": "ND"
    },
    {
        "name": "Northern Mariana Islands",
        "abbreviation": "MP"
    },
    {
        "name": "Ohio",
        "abbreviation": "OH"
    },
    {
        "name": "Oklahoma",
        "abbreviation": "OK"
    },
    {
        "name": "Oregon",
        "abbreviation": "OR"
    },
    {
        "name": "Palau",
        "abbreviation": "PW"
    },
    {
        "name": "Pennsylvania",
        "abbreviation": "PA"
    },
    {
        "name": "Puerto Rico",
        "abbreviation": "PR"
    },
    {
        "name": "Rhode Island",
        "abbreviation": "RI"
    },
    {
        "name": "South Carolina",
        "abbreviation": "SC"
    },
    {
        "name": "South Dakota",
        "abbreviation": "SD"
    },
    {
        "name": "Tennessee",
        "abbreviation": "TN"
    },
    {
        "name": "Texas",
        "abbreviation": "TX"
    },
    {
        "name": "Utah",
        "abbreviation": "UT"
    },
    {
        "name": "Vermont",
        "abbreviation": "VT"
    },
    {
        "name": "Virgin Islands",
        "abbreviation": "VI"
    },
    {
        "name": "Virginia",
        "abbreviation": "VA"
    },
    {
        "name": "Washington",
        "abbreviation": "WA"
    },
    {
        "name": "West Virginia",
        "abbreviation": "WV"
    },
    {
        "name": "Wisconsin",
        "abbreviation": "WI"
    },
    {
        "name": "Wyoming",
        "abbreviation": "WY"
    },
    {
        "name": "N/A",
        "abbreviation" : "N/A"
    }
];



}]);