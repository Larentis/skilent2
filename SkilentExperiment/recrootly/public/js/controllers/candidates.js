angular.module('mean.candidates').controller('CandidatesController', ['$scope', '$routeParams', '$location', '$anchorScroll', 'Global', 'Candidates', '$window', '$upload', '$http', 'promiseTracker', function ($scope, $routeParams, $location, $anchorScroll, Global, Candidates, $window, $upload, $http, promiseTracker) {
    $scope.global = Global;
    // initialize variables used for create candidate view
    $scope.candidate = {};
    $scope.message = {};
    $scope.newResume = {};
    $scope.isJobAppsActive = false;
    $scope.isNotesActive = false;
    $scope.newSkill = { name: '' };
    $scope.newNote = { text: '' };
    var urlBase = '/api/v1/candidates';

    $scope.create = function() {

        // Add note to candidate object
        $scope.addNoteToArray();      

        Candidates.insertCand($scope.candidate, $scope.myFile).success(function(response) {
            console.log("My new candidate id: " + response._id);

            //reset inputs
            for (var i in $scope.candidate){
                $scope.candidate[i] = '';               
            }
            $scope.candidate.skills = [];
            $scope.candidate.notes = [];

            $location.path("candidates/" + response._id);
        })
        .error(function(err, status) {
            console.log("Error inserting candidate");
            $scope.errorMessage = "Error inserting candidate. " + err;
        });

    };

    $scope.sendMessage = function() {
        var message = $scope.message;
        console.log(message);
    }


    $scope.update = function() {
        var candidate = $scope.candidate;

        $scope.errorMessage = '';

        Candidates.updateCand($routeParams.candidateId, candidate).success(function(response) {
            console.log("Updated candidate with ID: " + $routeParams.candidateId);
            $location.path('candidates/' + $routeParams.candidateId);
        })
        .error(function(err, status) {
            console.log("Error updating candidate: " + status + ": " + err );
            $scope.errorMessage = "Error updating candidate: " + status + ": " + err;
        });
    };


    $scope.insertNote = function() {

        if ($scope.newNote.text) {
            $scope.addNoteToArray();
            $scope.errorMessage = '';

            $http({
                method: 'PUT',
                url: urlBase + '/' + $routeParams.candidateId + '/notes', 
                data: $scope.candidate.notes[$scope.candidate.notes.length-1], 
                tracker: 'noteTracker'
            }).success(function(response) {
                $scope.newNote.text = '';
            })
            .error(function(err, status) {
                $scope.candidate.notes.pop();
                $scope.errorMessage = "Error inserting note: " + status + ": " + JSON.stringify(err);
            });
        } 
    };

    $scope.find = function() {
        Candidates.getAll().success(function(data, status) {
            $scope.candidates = data;
        })
        .error(function(err, status) {
            $scope.errorMessage = "Candidates list not retrieved. " +  err;
            console.log("Error retrieving candidates list");
        });
    };

    $scope.findOne = function() {
        if ($routeParams.viewApps)
        {
            $scope.isJobAppsActive = true;
        } else if ($routeParams.viewNotes) {
            $scope.isNotesActive = true;
        }   
        Candidates.getCand($routeParams.candidateId).success(function(data, status) {
            $scope.candidate = data;
        });
    };

    $scope.remove = function(candidate) {

        var deleteUser = $window.confirm('Are you sure you want to delete this user?');   

        if (deleteUser) {
            if (candidate) {
                console.log("Found candidate to remove from list");
                console.log("candidate data: " + candidate.name.first);
                // candidate.remove();  

                // for (var i in $scope.candidates) {
                //     if ($scope.candidates[i] == candidate) {
                //         $scope.candidates.splice(i, 1);
                //     }
                // }
            }
            else {
                console.log("Removing single candidate, not from list");
                //$scope.candidate.remove();
                $location.path('candidates');
            }
        }
    };

    $scope.modalremove = function() {
        $scope.errorMessage = '';
        candidate = $scope.markForDeletion;
        if (candidate) {
            console.log("Removing from list, candidate Id: " + candidate._id);
            Candidates.remove(candidate).success(function(data, status) {  
                for (var i in $scope.candidates) {
                    if ($scope.candidates[i] == candidate) {
                        $scope.candidates.splice(i, 1);
                    }
                }
            })
            .error(function(data, status, headers, config) {
                console.log("data is: " + data);
                console.log("status is: " + status);
                $scope.errorMessage = "Error removing candidate: " + status + ": " + data;
            });
        }
        //else {
        //    console.log("Removing single candidate, not from list");
        //    Candidates.remove($scope.candidate._id, $scope.candidate);
        //    $location.path('candidates');
        //}

    };    

    $scope.selectCandidate = function(candidate) {
        $scope.markForDeletion = candidate;
    }; 


    $scope.addSkill = function() {
        if ($scope.newSkill.name) {
            var partsOfStr = $scope.newSkill.name.split(',');
            for (var i in partsOfStr) {
                if (!$scope.candidate.skills) {
                    $scope.candidate.skills = [];
                }            
                $scope.candidate.skills.push(partsOfStr[i]);
            }
            $scope.newSkill.name = '';
        }
    };


    $scope.removeSkill = function(skill) {
        var i = $scope.candidate.skills.indexOf(skill);
        if(i != -1) {
            $scope.candidate.skills.splice(i, 1);
        }
    };


    $scope.addNote = function() {
        $scope.addNoteToArray();
        $scope.newNote.text = '';        
    };

    $scope.removeNote = function(note) {
        var i = $scope.candidate.notes.indexOf(note);
        if(i != -1) {
            $scope.candidate.notes.splice(i, 1);
        }
    };


    $scope.addNoteToArray = function() {
        // Add note to candidate object
        if ($scope.newNote.text) {
            if (!$scope.candidate.notes) {
                $scope.candidate.notes = [];
            }
            var myNote = {};
            myNote.createdBy = Global.user.name.first + ' ' + Global.user.name.last;
            myNote.created = new Date();
            myNote.noteText = $scope.newNote.text; 
            $scope.candidate.notes.push(myNote);
        }
        console.log('Hello');
    };

    $scope.scrollTo = function(myanchor, event) {
        console.log("Event: " + event);
        event.preventDefault();
        event.stopPropagation();        
        $location.hash(myanchor);
        $anchorScroll();
    };

 
    $scope.onFileSelect = function($files) {

        var s3Url = '';

        // retrive s3 policy from server
        $http.get('/api/v1/s3Options').then(function(res) {

            if (window.location.host.match(/localhost/)) {
                s3Url = "http://skilent-dev.s3.amazonaws.com";
            } else {
                s3Url = "http://skilent.s3.amazonaws.com";
            } 

            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];

                var destPath = 'resumes/' + Date.now() + "_" + file.name;                

                $scope.newResume = {
                    filename : file.name,
                    path : s3Url + '/' + destPath,
                    size : file.size,
                    contentType : file.type          
                };

                var formData = {
                    AWSAccessKeyId: res.data.s3Key,
                    policy: res.data.s3Policy,
                    signature: res.data.s3Signature,
                    acl: 'public-read',
                    'Content-Type': file.type,
                    key: destPath,
                };

                $scope.upload = $upload.upload({
                    url: s3Url, 
                    method: 'POST',
                    data: formData,
                    file: file,
                    // file: $files, //upload multiple files, this feature only works in HTML5 FromData browsers
                    /* set file formData name for 'Content-Desposition' header. Default: 'file' */
                    //fileFormDataName: myFile, //OR for HTML5 multiple upload only a list: ['name1', 'name2', ...]
                })
                .progress(fileProgress)
                .success(fileSuccess)
                .error(fileError);
                //.then(success, error, progress); 
            }
        }, function(res) {
          // error handler
          console.log('Error retrieving S3 Options from server: ' + res);
          $scope.s3ErrorMessage = 'Error retrieving S3 Options from server: ' + res;
        });
    };


    ///////////////////////////////////////////////////////////////////////////////////////
    //  Pull functions outside of loop bc of JSLint, this also has performance benefits 
    // see http://jsperf.com/closure-vs-name-function-in-a-loop/2
    ////////////////////////////////////////////////////////////////////////////////////////
    function fileProgress(evt) {
        $scope.dynamic = parseInt(100.0 * evt.loaded / evt.total);
        //console.log('percent: ' + myProgress);
    }

    function fileSuccess(data, status, headers, config) {
        // file is uploaded successfully
        $scope.candidate.resume = $scope.newResume;
        console.log("File uploaded to S3 successfully:" + data);

    }   

    function fileError(err, status) {
        $scope.s3ErrorMessage = "Error streaming file to S3: " + status + ": " + err;
        console.log("Error streaming file to S3: " + status + ": " + err );
    } 

//////////////////////////////////////////////////////////////
// Market Filter Buttons
//////////////////////////////////////////////////////////////
    $scope.selectedMarket = _.pluck($scope.comtechMarkets, 'code');

    $scope.setSelectedMarket = function (code, evt) {
        console.log("My market id: " + code);
        if (_.contains($scope.selectedMarket, code)) {
            $scope.selectedMarket = _.without($scope.selectedMarket, code);
        } else {
            $scope.selectedMarket.push(code);
        }
        evt.stopPropagation();
        return false;
    };

    $scope.isMarketChecked = function (id) {
        if (_.contains($scope.selectedMarket, id)) {
            return 'glyphicon glyphicon-ok pull-right';
        }
        return false;
    };

    $scope.checkAllMarkets = function () {
        $scope.selectedMarket = _.pluck($scope.comtechMarkets, 'code');
    };

    $scope.filterByMarket = function(job) {
        return ($scope.selectedMarket.indexOf(job.location.market) !== -1);
    };
    
//////////////////////////////////////////////////////////////
// Static Values -- To be Created as Service [in the future]
//////////////////////////////////////////////////////////////


    $scope.mainList = [
        {
            "name" : "NEW",
            "code" : "NEW"
        },
        {
            "name" : "In process",
            "code" : "In process"
        },
        {
            "name" : "Talent Pool",
            "code" : "Talent Pool"
        },
        {
            "name" : "HIRED",
            "code" : "HIRED"
        },
        {
            "name" : "Do Not Pursue",
            "code" : "Do Not Pursue"
        }
    ];

    $scope.sourceList = [
        {
            "name" : "Job Board",
            "code" : "Job Board"
        },
        {
            "name" : "Company Website",
            "code" : "Company Website"
        },
        {
            "name" : "LinkedIn",
            "code" : "LinkedIn"
        },
        {
            "name" : "Employee Referral",
            "code" : "Employee Referral"
        },
        {
            "name" : "Social Sourcing",
            "code" : "Social Sourcing"
        },
        {
            "name" : "Third Party",
            "code" : "Third Party"
        },
        {
            "name" : "State Unemployment Site",
            "code" : "State Unemployment Site"
        },
        {
            "name" : "Other",
            "code" : "Other"
        }
    ];

    $scope.sourceDetails = [
        {
            "name" : "Monster",
            "code" : "Monster"
        },
        {
            "name" : "Dice",
            "code" : "Dice"
        },
        {
            "name" : "CareerBuilder",
            "code" : "CareerBuilder"
        },
        {
            "name" : "Other",
            "code" : "Other"
        }
    ];

    $scope.comtechMarkets = [
        {name: 'United States', code: 'US'},
        {name: 'India', code: 'IN'},
        {name: 'Afghanistan', code: 'AF'},
        {name: 'Åland Islands', code: 'AX'},
        {name: 'Albania', code: 'AL'},
        {name: 'Algeria', code: 'DZ'},
        {name: 'American Samoa', code: 'AS'},
        {name: 'Andorra', code: 'AD'},
        {name: 'Angola', code: 'AO'},
        {name: 'Anguilla', code: 'AI'},
        {name: 'Antarctica', code: 'AQ'},
        {name: 'Antigua and Barbuda', code: 'AG'},
        {name: 'Argentina', code: 'AR'},
        {name: 'Armenia', code: 'AM'},
        {name: 'Aruba', code: 'AW'},
        {name: 'Australia', code: 'AU'},
        {name: 'Austria', code: 'AT'},
        {name: 'Azerbaijan', code: 'AZ'},
        {name: 'Bahamas', code: 'BS'},
        {name: 'Bahrain', code: 'BH'},
        {name: 'Bangladesh', code: 'BD'},
        {name: 'Barbados', code: 'BB'},
        {name: 'Belarus', code: 'BY'},
        {name: 'Belgium', code: 'BE'},
        {name: 'Belize', code: 'BZ'},
        {name: 'Benin', code: 'BJ'},
        {name: 'Bermuda', code: 'BM'},
        {name: 'Bhutan', code: 'BT'},
        {name: 'Bolivia', code: 'BO'},
        {name: 'Bosnia and Herzegovina', code: 'BA'},
        {name: 'Botswana', code: 'BW'},
        {name: 'Bouvet Island', code: 'BV'},
        {name: 'Brazil', code: 'BR'},
        {name: 'British Indian Ocean Territory', code: 'IO'},
        {name: 'Brunei Darussalam', code: 'BN'},
        {name: 'Bulgaria', code: 'BG'},
        {name: 'Burkina Faso', code: 'BF'},
        {name: 'Burundi', code: 'BI'},
        {name: 'Cambodia', code: 'KH'},
        {name: 'Cameroon', code: 'CM'},
        {name: 'Canada', code: 'CA'},
        {name: 'Cape Verde', code: 'CV'},
        {name: 'Cayman Islands', code: 'KY'},
        {name: 'Central African Republic', code: 'CF'},
        {name: 'Chad', code: 'TD'},
        {name: 'Chile', code: 'CL'},
        {name: 'China', code: 'CN'},
        {name: 'Christmas Island', code: 'CX'},
        {name: 'Cocos (Keeling) Islands', code: 'CC'},
        {name: 'Colombia', code: 'CO'},
        {name: 'Comoros', code: 'KM'},
        {name: 'Congo', code: 'CG'},
        {name: 'Congo, The Democratic Republic of the', code: 'CD'},
        {name: 'Cook Islands', code: 'CK'},
        {name: 'Costa Rica', code: 'CR'},
        {name: 'Cote D\'Ivoire', code: 'CI'},
        {name: 'Croatia', code: 'HR'},
        {name: 'Cuba', code: 'CU'},
        {name: 'Cyprus', code: 'CY'},
        {name: 'Czech Republic', code: 'CZ'},
        {name: 'Denmark', code: 'DK'},
        {name: 'Djibouti', code: 'DJ'},
        {name: 'Dominica', code: 'DM'},
        {name: 'Dominican Republic', code: 'DO'},
        {name: 'Ecuador', code: 'EC'},
        {name: 'Egypt', code: 'EG'},
        {name: 'El Salvador', code: 'SV'},
        {name: 'Equatorial Guinea', code: 'GQ'},
        {name: 'Eritrea', code: 'ER'},
        {name: 'Estonia', code: 'EE'},
        {name: 'Ethiopia', code: 'ET'},
        {name: 'Falkland Islands (Malvinas)', code: 'FK'},
        {name: 'Faroe Islands', code: 'FO'},
        {name: 'Fiji', code: 'FJ'},
        {name: 'Finland', code: 'FI'},
        {name: 'France', code: 'FR'},
        {name: 'French Guiana', code: 'GF'},
        {name: 'French Polynesia', code: 'PF'},
        {name: 'French Southern Territories', code: 'TF'},
        {name: 'Gabon', code: 'GA'},
        {name: 'Gambia', code: 'GM'},
        {name: 'Georgia', code: 'GE'},
        {name: 'Germany', code: 'DE'},
        {name: 'Ghana', code: 'GH'},
        {name: 'Gibraltar', code: 'GI'},
        {name: 'Greece', code: 'GR'},
        {name: 'Greenland', code: 'GL'},
        {name: 'Grenada', code: 'GD'},
        {name: 'Guadeloupe', code: 'GP'},
        {name: 'Guam', code: 'GU'},
        {name: 'Guatemala', code: 'GT'},
        {name: 'Guernsey', code: 'GG'},
        {name: 'Guinea', code: 'GN'},
        {name: 'Guinea-Bissau', code: 'GW'},
        {name: 'Guyana', code: 'GY'},
        {name: 'Haiti', code: 'HT'},
        {name: 'Heard Island and Mcdonald Islands', code: 'HM'},
        {name: 'Holy See (Vatican City State)', code: 'VA'},
        {name: 'Honduras', code: 'HN'},
        {name: 'Hong Kong', code: 'HK'},
        {name: 'Hungary', code: 'HU'},
        {name: 'Iceland', code: 'IS'},
        {name: 'Indonesia', code: 'ID'},
        {name: 'Iran, Islamic Republic Of', code: 'IR'},
        {name: 'Iraq', code: 'IQ'},
        {name: 'Ireland', code: 'IE'},
        {name: 'Isle of Man', code: 'IM'},
        {name: 'Israel', code: 'IL'},
        {name: 'Italy', code: 'IT'},
        {name: 'Jamaica', code: 'JM'},
        {name: 'Japan', code: 'JP'},
        {name: 'Jersey', code: 'JE'},
        {name: 'Jordan', code: 'JO'},
        {name: 'Kazakhstan', code: 'KZ'},
        {name: 'Kenya', code: 'KE'},
        {name: 'Kiribati', code: 'KI'},
        {name: 'Korea, Democratic People\'S Republic of', code: 'KP'},
        {name: 'Korea, Republic of', code: 'KR'},
        {name: 'Kuwait', code: 'KW'},
        {name: 'Kyrgyzstan', code: 'KG'},
        {name: 'Lao People\'S Democratic Republic', code: 'LA'},
        {name: 'Latvia', code: 'LV'},
        {name: 'Lebanon', code: 'LB'},
        {name: 'Lesotho', code: 'LS'},
        {name: 'Liberia', code: 'LR'},
        {name: 'Libyan Arab Jamahiriya', code: 'LY'},
        {name: 'Liechtenstein', code: 'LI'},
        {name: 'Lithuania', code: 'LT'},
        {name: 'Luxembourg', code: 'LU'},
        {name: 'Macao', code: 'MO'},
        {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'},
        {name: 'Madagascar', code: 'MG'},
        {name: 'Malawi', code: 'MW'},
        {name: 'Malaysia', code: 'MY'},
        {name: 'Maldives', code: 'MV'},
        {name: 'Mali', code: 'ML'},
        {name: 'Malta', code: 'MT'},
        {name: 'Marshall Islands', code: 'MH'},
        {name: 'Martinique', code: 'MQ'},
        {name: 'Mauritania', code: 'MR'},
        {name: 'Mauritius', code: 'MU'},
        {name: 'Mayotte', code: 'YT'},
        {name: 'Mexico', code: 'MX'},
        {name: 'Micronesia, Federated States of', code: 'FM'},
        {name: 'Moldova, Republic of', code: 'MD'},
        {name: 'Monaco', code: 'MC'},
        {name: 'Mongolia', code: 'MN'},
        {name: 'Montserrat', code: 'MS'},
        {name: 'Morocco', code: 'MA'},
        {name: 'Mozambique', code: 'MZ'},
        {name: 'Myanmar', code: 'MM'},
        {name: 'Namibia', code: 'NA'},
        {name: 'Nauru', code: 'NR'},
        {name: 'Nepal', code: 'NP'},
        {name: 'Netherlands', code: 'NL'},
        {name: 'Netherlands Antilles', code: 'AN'},
        {name: 'New Caledonia', code: 'NC'},
        {name: 'New Zealand', code: 'NZ'},
        {name: 'Nicaragua', code: 'NI'},
        {name: 'Niger', code: 'NE'},
        {name: 'Nigeria', code: 'NG'},
        {name: 'Niue', code: 'NU'},
        {name: 'Norfolk Island', code: 'NF'},
        {name: 'Northern Mariana Islands', code: 'MP'},
        {name: 'Norway', code: 'NO'},
        {name: 'Oman', code: 'OM'},
        {name: 'Pakistan', code: 'PK'},
        {name: 'Palau', code: 'PW'},
        {name: 'Palestinian Territory, Occupied', code: 'PS'},
        {name: 'Panama', code: 'PA'},
        {name: 'Papua New Guinea', code: 'PG'},
        {name: 'Paraguay', code: 'PY'},
        {name: 'Peru', code: 'PE'},
        {name: 'Philippines', code: 'PH'},
        {name: 'Pitcairn', code: 'PN'},
        {name: 'Poland', code: 'PL'},
        {name: 'Portugal', code: 'PT'},
        {name: 'Puerto Rico', code: 'PR'},
        {name: 'Qatar', code: 'QA'},
        {name: 'Reunion', code: 'RE'},
        {name: 'Romania', code: 'RO'},
        {name: 'Russian Federation', code: 'RU'},
        {name: 'Rwanda', code: 'RW'},
        {name: 'Saint Helena', code: 'SH'},
        {name: 'Saint Kitts and Nevis', code: 'KN'},
        {name: 'Saint Lucia', code: 'LC'},
        {name: 'Saint Pierre and Miquelon', code: 'PM'},
        {name: 'Saint Vincent and the Grenadines', code: 'VC'},
        {name: 'Samoa', code: 'WS'},
        {name: 'San Marino', code: 'SM'},
        {name: 'Sao Tome and Principe', code: 'ST'},
        {name: 'Saudi Arabia', code: 'SA'},
        {name: 'Senegal', code: 'SN'},
        {name: 'Serbia and Montenegro', code: 'CS'},
        {name: 'Seychelles', code: 'SC'},
        {name: 'Sierra Leone', code: 'SL'},
        {name: 'Singapore', code: 'SG'},
        {name: 'Slovakia', code: 'SK'},
        {name: 'Slovenia', code: 'SI'},
        {name: 'Solomon Islands', code: 'SB'},
        {name: 'Somalia', code: 'SO'},
        {name: 'South Africa', code: 'ZA'},
        {name: 'South Georgia and the South Sandwich Islands', code: 'GS'},
        {name: 'Spain', code: 'ES'},
        {name: 'Sri Lanka', code: 'LK'},
        {name: 'Sudan', code: 'SD'},
        {name: 'Suriname', code: 'SR'},
        {name: 'Svalbard and Jan Mayen', code: 'SJ'},
        {name: 'Swaziland', code: 'SZ'},
        {name: 'Sweden', code: 'SE'},
        {name: 'Switzerland', code: 'CH'},
        {name: 'Syrian Arab Republic', code: 'SY'},
        {name: 'Taiwan, Province of China', code: 'TW'},
        {name: 'Tajikistan', code: 'TJ'},
        {name: 'Tanzania, United Republic of', code: 'TZ'},
        {name: 'Thailand', code: 'TH'},
        {name: 'Timor-Leste', code: 'TL'},
        {name: 'Togo', code: 'TG'},
        {name: 'Tokelau', code: 'TK'},
        {name: 'Tonga', code: 'TO'},
        {name: 'Trinidad and Tobago', code: 'TT'},
        {name: 'Tunisia', code: 'TN'},
        {name: 'Turkey', code: 'TR'},
        {name: 'Turkmenistan', code: 'TM'},
        {name: 'Turks and Caicos Islands', code: 'TC'},
        {name: 'Tuvalu', code: 'TV'},
        {name: 'Uganda', code: 'UG'},
        {name: 'Ukraine', code: 'UA'},
        {name: 'United Arab Emirates', code: 'AE'},
        {name: 'United Kingdom', code: 'GB'},
        {name: 'United States Minor Outlying Islands', code: 'UM'},
        {name: 'Uruguay', code: 'UY'},
        {name: 'Uzbekistan', code: 'UZ'},
        {name: 'Vanuatu', code: 'VU'},
        {name: 'Venezuela', code: 'VE'},
        {name: 'Viet Nam', code: 'VN'},
        {name: 'Virgin Islands, British', code: 'VG'},
        {name: 'Virgin Islands, U.S.', code: 'VI'},
        {name: 'Wallis and Futuna', code: 'WF'},
        {name: 'Western Sahara', code: 'EH'},
        {name: 'Yemen', code: 'YE'},
        {name: 'Zambia', code: 'ZM'},
        {name: 'Zimbabwe', code: 'ZW'}
    ];

    $scope.comtechEducation = [
        {
            "name" : "High school diploma / GED",
            "code" : "High school diploma / GED"
        },
        {
            "name" : "Associate Degree",
            "code" : "Associate Degree"
        },
        {
            "name" : "Bachelor's Degree",
            "code" : "Bachelor's Degree"
        },
        {
            "name" : "Master's degree",
            "code" : "Master's degree"
        },
        {
            "name" : "Doctorate",
            "code" : "Doctorate"
        },
        {
            "name" : "Other",
            "code" : "Other"
        }
    ];

    $scope.auth = [
        {
            "name" : "Authorized to work in the US for any employer",
            "code" : "Authorized for any employer"
        },
        {
            "name" : "Authorized to work in the US for present employer only",
            "code" : "Authorized for present employer"
        },
        {
            "name" : "Requires sponsorship to work in the US",
            "code" : "Requires sponsorship"
        },
        {
            "name" : "Status to work in the US unknown",
            "code" : "Unknown"
        }
    ];

    $scope.statusList = [
        {
            "name" : "Applied",
            "code" : "Applied"
        },
        {
            "name" : "Linked",
            "code" : "Linked"
        },
        {
            "name" : "Resume Reviewed",
            "code" : "Resume Reviewed"
        },
        {
            "name" : "Phone Screen",
            "code" : "Phone Screen"
        },
        {
            "name" : "Technical Phone Interview",
            "code" : "Technical Phone Interview"
        },
        {
            "name" : "In Person Interview",
            "code" : "In Person Interview"
        },
        {
            "name" : "Verbal Offer",
            "code" : "Verbal Offer"
        },
        {
            "name" : "Written Offer",
            "code" : "Written Offer"
        },
        {
            "name" : "On Boarding Process",
            "code" : "On Boarding Process"
        }

    ];

    $scope.onboard = [
        {
            "name" : "Completed - Successful",
            "code" : "Completed - Successful"
        },
        {
            "name" : "Completed - Unsuccessful",
            "code" : "Completed - Unsuccessful"
        },
        {
            "name" : "Hired",
            "code" : "Hired"
        }
    ];

    $scope.rejectedDropdown = [
        {
            "name" : "Rejected Offer",
            "code" : "Rejected Offer"
        },
        {
            "name" : "Job Requisition Cancelled",
            "code" : "Job Requisition Cancelled"
        },
        {
            "name" : "Does Not Meet Basic Qualifications",
            "code" : "Does Not Meet Basic Qualifications"
        },
        {
            "name" : "More Qualified Candidate Selected",
            "code" : "More Qualified Candidate Selected"
        },
        {
            "name" : "Salary Requirements Do Not Match Job",
            "code" : "Salary Requirements Do Not Match Job"
        },
        {
            "name" : "Career Objectives Do Not Match Job",
            "code" : "Career Objectives Do Not Match Job"
        },
        {
            "name" : "Location Requirements Do Not Match Job",
            "code" : "Location Requirements Do Not Match Job"
        },
        {
            "name" : "Failed to return calls / request for information",
            "code" : "Failed to return calls / request for information"
        },
        {
            "name" : "Does not meet work authorization requirements",
            "code" : "Does not meet work authorization requirements"
        },
        {
            "name" : "Position Filled By Internal Candidate",
            "code" : "Position Filled By Internal Candidate"
        },
        {
            "name" : "Candidate Withdrew",
            "code" : "Candidate Withdrew"
        },
        {
            "name" : "No Show for Interview",
            "code" : "No Show for Interview"
        },
        {
            "name" : "Did not satisfy background / drug test requirements",
            "code" : "Did not satisfy background / drug test requirements"
        }
    ];

    $scope.offerStatus = [
        {
            "name" : "Extended",
            "code" : "Extended"
        },
        {
            "name" : "Accepted",
            "code" : "Accepted"
        },
        {
            "name" : "Rejected",
            "code" : "Rejected"
        }
    ];

    $scope.states = [
    {   
        "name" : "India",
        "abbreviation" : "India"
    },
    {
        "name": "Alabama",
        "abbreviation": "AL"
    },
    {
        "name": "Alaska",
        "abbreviation": "AK"
    },
    {
        "name": "American Samoa",
        "abbreviation": "AS"
    },
    {
        "name": "Arizona",
        "abbreviation": "AZ"
    },
    {
        "name": "Arkansas",
        "abbreviation": "AR"
    },
    {
        "name": "California",
        "abbreviation": "CA"
    },
    {
        "name": "Colorado",
        "abbreviation": "CO"
    },
    {
        "name": "Connecticut",
        "abbreviation": "CT"
    },
    {
        "name": "Delaware",
        "abbreviation": "DE"
    },
    {
        "name": "District Of Columbia",
        "abbreviation": "DC"
    },
    {
        "name": "Federated States Of Micronesia",
        "abbreviation": "FM"
    },
    {
        "name": "Florida",
        "abbreviation": "FL"
    },
    {
        "name": "Georgia",
        "abbreviation": "GA"
    },
    {
        "name": "Guam",
        "abbreviation": "GU"
    },
    {
        "name": "Hawaii",
        "abbreviation": "HI"
    },
    {
        "name": "Idaho",
        "abbreviation": "ID"
    },
    {
        "name": "Illinois",
        "abbreviation": "IL"
    },
    {
        "name": "Indiana",
        "abbreviation": "IN"
    },
    {
        "name": "Iowa",
        "abbreviation": "IA"
    },
    {
        "name": "Kansas",
        "abbreviation": "KS"
    },
    {
        "name": "Kentucky",
        "abbreviation": "KY"
    },
    {
        "name": "Louisiana",
        "abbreviation": "LA"
    },
    {
        "name": "Maine",
        "abbreviation": "ME"
    },
    {
        "name": "Marshall Islands",
        "abbreviation": "MH"
    },
    {
        "name": "Maryland",
        "abbreviation": "MD"
    },
    {
        "name": "Massachusetts",
        "abbreviation": "MA"
    },
    {
        "name": "Michigan",
        "abbreviation": "MI"
    },
    {
        "name": "Minnesota",
        "abbreviation": "MN"
    },
    {
        "name": "Mississippi",
        "abbreviation": "MS"
    },
    {
        "name": "Missouri",
        "abbreviation": "MO"
    },
    {
        "name": "Montana",
        "abbreviation": "MT"
    },
    {
        "name": "Nebraska",
        "abbreviation": "NE"
    },
    {
        "name": "Nevada",
        "abbreviation": "NV"
    },
    {
        "name": "New Hampshire",
        "abbreviation": "NH"
    },
    {
        "name": "New Jersey",
        "abbreviation": "NJ"
    },
    {
        "name": "New Mexico",
        "abbreviation": "NM"
    },
    {
        "name": "New York",
        "abbreviation": "NY"
    },
    {
        "name": "North Carolina",
        "abbreviation": "NC"
    },
    {
        "name": "North Dakota",
        "abbreviation": "ND"
    },
    {
        "name": "Northern Mariana Islands",
        "abbreviation": "MP"
    },
    {
        "name": "Ohio",
        "abbreviation": "OH"
    },
    {
        "name": "Oklahoma",
        "abbreviation": "OK"
    },
    {
        "name": "Oregon",
        "abbreviation": "OR"
    },
    {
        "name": "Palau",
        "abbreviation": "PW"
    },
    {
        "name": "Pennsylvania",
        "abbreviation": "PA"
    },
    {
        "name": "Puerto Rico",
        "abbreviation": "PR"
    },
    {
        "name": "Rhode Island",
        "abbreviation": "RI"
    },
    {
        "name": "South Carolina",
        "abbreviation": "SC"
    },
    {
        "name": "South Dakota",
        "abbreviation": "SD"
    },
    {
        "name": "Tennessee",
        "abbreviation": "TN"
    },
    {
        "name": "Texas",
        "abbreviation": "TX"
    },
    {
        "name": "Utah",
        "abbreviation": "UT"
    },
    {
        "name": "Vermont",
        "abbreviation": "VT"
    },
    {
        "name": "Virgin Islands",
        "abbreviation": "VI"
    },
    {
        "name": "Virginia",
        "abbreviation": "VA"
    },
    {
        "name": "Washington",
        "abbreviation": "WA"
    },
    {
        "name": "West Virginia",
        "abbreviation": "WV"
    },
    {
        "name": "Wisconsin",
        "abbreviation": "WI"
    },
    {
        "name": "Wyoming",
        "abbreviation": "WY"
    },
    {
        "name": "N/A",
        "abbreviation" : "N/A"
    }
];

}]);