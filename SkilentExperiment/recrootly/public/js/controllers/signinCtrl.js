/*
 * client/js/auth/controllers/signinCtrl.js
 */
angular.module('mean.users').controller('signinCtrl', ['$scope', 'Global', 'UserServices', function ($scope, Global, UserServices) {

    $scope.global = Global;

    $scope.focus = {
      username: true
    };
    $scope.showError = false;
    $scope.amiin = "initializing...";

    $scope.login = function (formData, formMeta) {
      $scope.amiin= "in the login function";

      if (formMeta.$invalid) {
        $scope.showError = true;
        var fields = ['email', 'password'];
        var erroredField = _.find(fields, function (field) {
          return formMeta[field].$invalid;
        });
        $scope.focus[erroredField] = true;
        return;
      }

      // Users.signin(formData).then(function () {
      //   $scope.showError = false;
      //   alert.clearMessages();
      //   $scope.amiin = "Yes i am";
      // }, function (res) {
      //   $scope.amiin="No i am not";
      //   $scope.showError = true;
      //   alert.setMessages('danger', res.data.result.messages);
      // });
    };
}]);
