/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('underscore'),
    crypto = require("crypto"),  
    moment = require('moment-timezone'),      
    config = require('../../config/config');

/**
 * Amazon s3 policy, signature for client-side S3 uploads
 */
exports.s3Options = function(req, res) {
  var date = new Date();

  var s3Policy = {
    "expiration": moment.utc(moment().add('h', 1)).toISOString(),
    "conditions": [
      {"bucket": config.s3.bucket},    
      ["starts-with", "$key", ""],  
      {"acl": "public-read"}, 
      ["starts-with", "$Content-Type", ""],
      ["content-length-range", 0, 10 * 1024 * 1024],
    ]
  };

  // stringify and encode the policy
  var stringPolicy = JSON.stringify(s3Policy);
  var base64Policy = Buffer(stringPolicy, "utf8").toString("base64");

  // sign the base64 encoded policy
  var signature = crypto.createHmac("sha1", config.s3.secret)
    .update(new Buffer(base64Policy, "utf8")).digest("base64");

  // build the results object
  var s3Credentials = {
    s3Policy: base64Policy,
    s3Signature: signature,
    s3Key: config.s3.key
  };

  // send it back
  res.json(s3Credentials);
};

