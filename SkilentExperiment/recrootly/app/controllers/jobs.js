/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Job = mongoose.model('Job'),
    _ = require('underscore'),
    config = require('../../config/config');



/**
 * Find job by id
 */
exports.job = function(req, res, next, id) {
    Job.load(id, function(err, job) {
        if (err) return next(err);
        if (!job) return next(new Error('Failed to load job ' + id));
        req.job = job;
        next();
    });
};

/**
 * Create a job
 */
exports.create = function(req, res) {
    var job = new Job();
    job.user = req.user.id;
    job.updatedBy = req.user.id;

    // clone object using underscore util
    job = _.extend(job, req.body);

    job.save(function(err) {
      if (err) {
        return res.send(400, { errors: err.errors });
      } else {
        res.jsonp(job);
      }
    });
};



/**
 * Update a job
 */
exports.update = function(req, res) {
    var job = req.job;
    // clone object using underscore util
    job = _.extend(job, req.body);
    job.updated = Date.now();
    job.updatedBy = req.user.id;

    job.save(function(err) {
        if (err) {
            return res.send(400, { errors: err });
        } else {
            res.json(job);
        }
    });
};

/**
 * Delete an job
 */
exports.destroy = function(req, res) {
    var job = req.job;

    job.remove(function(err) {
        if (err) {
            console.log("My error: " + err);
            res.send(500, { errors: err });
        } else {
            res.json(job);
        }
    });
};

/**
 * Show a job
 */
exports.show = function(req, res) {
    res.json(req.job);
};

/**
 * List of all jobs
 */
exports.all = function(req, res) {
    Job.find().sort('-created').populate('user', 'name email').populate('leadRec', 'name').exec(function(err, jobs) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.json(jobs);
        }
    });
};


/**
 * List of all jobs with status Open - Funded (Jobs that seek applicants)
 */
exports.allopen = function(req, res) {
    Job.find({ 'status': 'Open - Funded' }).sort('-created').populate('user', 'name email').populate('leadRec', 'name').exec(function(err, jobs) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.json(jobs);
        }
    });
};
