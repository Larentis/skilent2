angular.module('mean.system').controller('SocialSearchCtrl', ['$scope', '$http', '$q', '$location', 'geolocation', 'promiseTracker', 'Geocoder', function ($scope, $http, $q, location, geolocation, promiseTracker, Geocoder) {

    // initialize the map
    // angular.extend($scope, {
    //     current: {
    //         lat: 10,
    //         lng: 10,
    //         zoom: 10
    //     },
    //     markers: {},
    //     defaults: {
    //         scrollWheelZoom: false
    //     }                  
    // });

    // initialize the form variable
    $scope.myForm = {};
    $scope.currentPage = 0;
    $scope.ghCurrentPage = 0;
    $scope.seCurrentPage = 0;
    $scope.pageSize = 7;
    $scope.ghPageSize = 7;
    $scope.sePageSize = 7;
    $scope.results = [];
    $scope.ghResults = [];
    $scope.tagResults = [];
    $scope.serverTagResults = [];
    $scope.seTopAnswererResults = [];
    $scope.rlength = 0;
    $scope.ghRlength = 0;
    $scope.seRlength = 0;
    $scope.geo = {};
    $scope.showResults = false;
    $scope.showGhResults = false;
    $scope.showTagResults = false;
    $scope.seErrorMessage = '';
    $scope.twErrorMessage = '';
    $scope.ghErrorMessage= '';
    $scope.geo.radius = "30mi"; 
    $scope.coords = {};   
    //$scope.markers = new Array();

    //Create / get our tracker with unique ID
    $scope.searchTracker = promiseTracker('searchTracker'); 

    $scope.searchSocial = function() {

      // make sure the searchTerm has been provided
      if ($scope.myForm.searchTerm && $scope.myForm.searchTerm.length > 0)
      {
        // First geocode location
        if ($scope.myForm.location && $scope.myForm.location.length > 0) {

          Geocoder.geoForAddress($scope.myForm.location).then(function(response) {

            $scope.coords.lat = response.lat; 
            $scope.coords.lng = response.lng;               
            //$scope.gotoLocation(loc.lat(), loc.lng());

            $scope.searchTwitter();
            $scope.searchGithub();
            //$scope.searchRelatedTags();
            //$scope.searchRelatedServerTags();
            $scope.searchTopAnswerers();

            // update location after sending results
            $scope.myForm.location = response.address; 

          }, function(response) {
            // error handler
            alert(JSON.stringify(response));
          });
        }
      }



      //   if (!this.geocoder) this.geocoder = new google.maps.Geocoder();
      //   this.geocoder.geocode({ 'address': $scope.myForm.location }, function (results, status) {
      //       if (status == google.maps.GeocoderStatus.OK) {
      //           $scope.myForm.location = results[0].formatted_address;
      //           $scope.coords.lat = results[0].geometry.location.lat(); 
      //           $scope.coords.lng = results[0].geometry.location.lng();                 
      //           //$scope.gotoLocation(loc.lat(), loc.lng());

      //           $scope.searchTwitter();
      //           $scope.searchRelatedTags();
      //           $scope.searchRelatedServerTags();
      //       } else {
      //           alert("Problem geocoding location.");
      //       }
      //   });
      // }

    };

    $scope.searchTwitter = function() {
      $scope.showResults = true;
      $http({method: 'GET', url: '/api/v1/twSearch', params:{
        searchTerm: $scope.myForm.searchTerm,
        lat: $scope.coords.lat,
        long: $scope.coords.lng,
        radius: $scope.geo.radius
        },
        tracker: 'searchTracker'
      }).
        success(function(data, status, headers, config) {
          $scope.rlength = data.statuses.length; 
          $scope.results = data;  
          $scope.currentPage = 0;
        }).
        error(function(data, status, headers, config) {
          $scope.results = [];
          $scope.twErrorMessage = JSON.stringify(data);
        });  
    };  


    $scope.searchGithub = function() {
      $scope.showGhResults = true;
      $http({method: 'GET', url: '/api/v1/ghSearch', params:{
        searchTerm: $scope.myForm.searchTerm,
        location: $scope.myForm.location
        },
        tracker: 'searchGhTracker'
      }).
        success(function(data, status, headers, config) {
          $scope.ghRlength = data.items.length;
          $scope.ghResults = data.items;
          $scope.ghCurrentPage = 0;           
        }).
        error(function(data, status, headers, config) {
          $scope.ghResults = [];
          $scope.ghErrorMessage = JSON.stringify(data);
        });  
    };     

    $scope.searchLinkedin = function() {
      $scope.showResults = true;
      $http({method: 'GET', url: '/api/v1/liSearch', params:{
        searchTerm: $scope.myForm.searchTerm
        },
        tracker: 'searchTracker'
      }).
        success(function(data, status, headers, config) {
          $scope.rlength = data.statuses.length; 
          $scope.results = data;  
          $scope.currentPage = 0;
        }).
        error(function(data, status, headers, config) {
          $scope.results = 'Error!';
        });  
    };     
    
    $scope.searchRelatedTags = function() {
      $scope.showTagResults = true;
      $http({method: 'GET', url: '/api/v1/seSearch/relatedTags', params:{
        searchTerm: $scope.myForm.searchTerm
        },
        tracker: 'searchTagsTracker'
      }).
        success(function(data, status, headers, config) {
          $scope.tagResults = data.items;  
        }).
        error(function(data, status, headers, config) {
          $scope.tagResults = null;
        });  
    };  


    $scope.searchRelatedServerTags = function() {
      $scope.showTagResults = true;
      $http({method: 'GET', url: '/api/v1/sfSearch/relatedTags', params:{
        searchTerm: $scope.myForm.searchTerm
        },
        tracker: 'searchServerTagsTracker'
      }).
        success(function(data, status, headers, config) {
          $scope.serverTagResults = data.items;  
        }).
        error(function(data, status, headers, config) {
          $scope.serverTagResults = null;
        });  
    }; 

    $scope.searchTopAnswerers = function() {    
      $scope.showTopAnswererResults = true;
      $http({method: 'GET', url: '/api/v1/seSearch/topTagAnswerers', params:{
        searchTerm: $scope.myForm.searchTerm
        },
        tracker: 'seTopAnswerersTracker'
      }).
        success(function(data, status, headers, config) {
          $scope.seRlength = data.items.length; 
          $scope.seCurrentPage = 0;          
          $scope.seTopAnswererResults = data.items;  
        }).
        error(function(data, status, headers, config) {
          $scope.seTopAnswererResults = [];
          $scope.seErrorMessage = JSON.stringify(data);
        });  
    }; 


    $scope.numberOfPages=function(){
        return Math.ceil($scope.rlength/$scope.pageSize);                
    };


    $scope.ghNumberOfPages=function(){
        return Math.ceil($scope.ghRlength/$scope.ghPageSize);                
    };

    $scope.seNumberOfPages=function(){
        return Math.ceil($scope.seRlength/$scope.sePageSize);                
    };

    $scope.previousClick = function() {
        if ($scope.currentPage !== 0) {
          $scope.currentPage = $scope.currentPage - 1;
        }
    };

    $scope.ghPreviousClick = function() {
        if ($scope.ghCurrentPage !== 0) {
          $scope.ghCurrentPage = $scope.ghCurrentPage - 1;
        }
    };

    $scope.sePreviousClick = function() {
        if ($scope.seCurrentPage !== 0) {
          $scope.seCurrentPage = $scope.seCurrentPage - 1;
        }
    };

    $scope.nextClick = function() {
        if ($scope.currentPage < (($scope.rlength / $scope.pageSize) - 1)) {
          $scope.currentPage = $scope.currentPage + 1;
        }
    };

    $scope.ghNextClick = function() {
        if ($scope.ghCurrentPage < (($scope.ghRlength / $scope.ghPageSize) - 1)) {
          $scope.ghCurrentPage = $scope.ghCurrentPage + 1;
        }
    };

    $scope.seNextClick = function() {
        if ($scope.seCurrentPage < (($scope.seRlength / $scope.sePageSize) - 1)) {
          $scope.seCurrentPage = $scope.seCurrentPage + 1;
        }
    };
    // geolocation.getLocation().then(function(data){
    //   angular.extend($scope, {
    //     current: {
    //         lat: data.coords.latitude,
    //         lng: data.coords.longitude,
    //         zoom: 10
    //             },
    //     markers: {
    //         mainMarker: {
    //               lat: data.coords.latitude,
    //               lng: data.coords.longitude,
    //               message: "My search location.<br>  Drag to update location parameters.",
    //               title: "Marker",
    //               focus: true,
    //               draggable: true
    //             }           
    //     },
    //     defaults: {
    //         scrollWheelZoom: false
    //     }           
    //   });

    //   $scope.geo.radius = "10mi";
    //   $scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
    // }); 

    $scope.addMarkers = function(mymarker) {
        $scope.markers.push({
            lat: mymarker.lat, 
            long: mymarker.long,
            message: "my marker"
        });
    };

    $scope.removeMarkers = function() {
        $scope.markers = {};
    };


}]);

