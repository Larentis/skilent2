/**
 * Module dependencies.
 */
var should = require('should'),
    app = require('../../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Job = mongoose.model('Job');

//Globals
var user;
var job;

//The tests
describe('<Unit Test>', function() {
    describe('Model Job:', function() {
        beforeEach(function(done) {
            user = new User({
                name: { 
                    first: 'First name',
                    last: 'Last name'
                },
                email: 'test@test.com',
                //username: 'user',
                password: 'password',
                provider: 'local',
                regStatus: 'Approved',
                updated: Date.now                
            });

            user.save(function(err) {
                job = new Job({
                    title: 'The best job ever',
                    projName: 'Facebook Web Development',
                    location: {
                        city: 'New York',
                        state: 'NY',
                        market: 'U.S.'
                    },
                    startDate: Date.now,
                    minExp: 2,
                    minEducation: 'High School diploma',
                    description: 'You will do it all and learn it all.',
                    workAuth: 'Citizen',
                    jobType: 'W2',
                    salary: {
                        hourlyRange: '55-65',
                        annualRange: '100,000-200,000',
                        public: true
                    },
                    signBonus: '2,000',
                    relo: '2,000',
                    telework: false,
                    skills: ['Java', 'Objective C'],
                    hiringManager: {
                        first: 'Randy',
                        last: 'Randius'
                    },
                    status: 'Open',
                    updated: Date.now,
                    user: user,
                    leadRec: user
                });
                done();
            });
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                return job.save(function(err) {
                    should.not.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without title', function(done) {
                job.title = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without project name', function(done) {
                job.projName = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without city location', function(done) {
                job.location.city = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without state location', function(done) {
                job.location.state = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without minimum requred experience', function(done) {
                job.minExp = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without minimum required education', function(done) {
                job.minEducation = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without job description', function(done) {
                job.description = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without work authorization', function(done) {
                job.workAuth = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without job type', function(done) {
                job.jobType = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without hiring manager first name', function(done) {
                job.hiringMgr.first = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without hiring manager last name', function(done) {
                job.hiringMgr.last = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without a status', function(done) {
                job.status = '';

                return job.save(function(err) {
                    should.exist(err);
                    done();
                });
            });
        });

        afterEach(function(done) {
            Job.remove({});
            User.remove({});
            done();
        });
        after(function(done){
            Job.remove().exec();
            User.remove().exec();
            done();
        });
    });
});
