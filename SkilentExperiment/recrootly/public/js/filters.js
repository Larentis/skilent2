//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
angular.module('mean.socialsearch').
  filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        if (typeof input != 'undefined') 
        {
            return input.slice(start);
        } else {
            return 0;
        }           
    };
  });

angular.module('mean.system').
  filter('reverse', function() {
    var items = [];
    return function(items) {
      if (items) {
        return items.slice().reverse();
      }
    };
});