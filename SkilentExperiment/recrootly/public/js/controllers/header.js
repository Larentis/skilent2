angular.module('mean.system').controller('HeaderController', ['$scope', 'Global', function ($scope, Global) {
	$scope.global = Global;

	$scope.menu = [{
		"title": "Candidates",
		"link": "candidates"
	}, {
		"title": "Jobs",
		"link": "jobs"
	}, {
		"title": "Applications",
		"link": "applications"
	}, {
		"title": "Social Search",
		"link": "socialsearch"
	}, {
		"title": "Settings",
		"link": "settings"
	}, {
		"title": "Roles",
		"link": "roles"
	}];



	$scope.isCollapsed = false;
}]);