/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Job = mongoose.model('Job'),
    Cand = mongoose.model('Candidate'),
    JApp = mongoose.model('Application'),
    _ = require('underscore'),
    ObjectId = require('mongoose').Types.ObjectId,
    multiparty = require('multiparty'),
    knox = require('knox'),
    moment = require('moment-timezone'),
    mailer = require('./mailer'),         
    config = require('../../config/config');


exports.fault = function (req, res) {
    console.log(req);
};
/**
 * Find job application by id
 */
exports.application = function(req, res, next, id) {
    JApp.load(id, function(err, application) {
        if (err) return next(err);
        if (!application) return next(new Error('Failed to load job application' + id));
        req.application = application;
        next();
    });
};

/**
 * Show a job application
 */
exports.show = function(req, res) {
    res.json(req.application);
};

/**
 * List of all job applications
 */
exports.all = function(req, res) {
    JApp.find().sort('-created').populate('user', 'name email').exec(function(err, applications) {
        if (err) {
            return res.send(400, { errors: err });
        } else {
            res.json(applications);
        }
    });
};

/**
 * Update a job application
 */
exports.update = function(req, res) {
    var application = req.application;
    application = _.extend(application, req.body);
    application.updated = Date.now(); 
    application.updatedBy = req.user.id; 

    application.save(function(err) {
        if (err) {
            return res.send(400, { errors: err });
        } else {
            res.json(204);
        }
    });
};


/**
 * Upsert a candidate and create application
 * This method is used for candidates applying from the web
 */
exports.create = function(req, res, next) {
    var candidate = new Cand();
    var jApp = new JApp();
    var appMeta;

    var s3Client = knox.createClient({
      secure: false,
      key: config.s3.key,
      secret: config.s3.secret,
      bucket: config.s3.bucket
    });
    var headers = {
      'x-amz-acl': 'public-read',
    };

    var form = new multiparty.Form();
    var done;

    form.on('field', function(name, value) {
        if (name === 'firstName') candidate.name.first = value;
        else if (name === 'lastName') candidate.name.last = value;
        else if (name === 'email') candidate.email = value;
        else if (name === 'phone') candidate.phone = value;
        else if (name === 'jobSource') jApp.jobSource = value;
        else if (name === 'jobId') jApp.job = new ObjectId(value);
        else if (name === 'jobTitle') jApp.jobTitle = value;
        else if (name === 'meta') appMeta = value;
    });

    form.on('part', function(part) {
        if (! part.filename) return;

        headers['Content-Length'] = part.byteCount;
        headers['Content-Type'] = part.headers['content-type'];
        var destPath = '/resumes/' + moment().format("MMDDYYYY-HHmmss") + "_" + part.filename;

       candidate.resume = {
            filename : part.filename,
            path : "https://" + config.s3.bucket + ".s3.amazonaws.com" + destPath,
            size : part.byteCount,
            contentType : part.headers['content-type']
        };

        s3Client.putStream(part, destPath, headers, function(s3Err, s3Response) {
            if (s3Err) next(s3Err);
            //res.statusCode = s3Response.statusCode;
            s3Response.resume();
            console.log("putStream to amazon");    
        });

    });


    form.on('error', function (formErr){
        if (formErr) next(formErr);
        done = true;
    });


    form.on('close', function (){
        console.log("My candidate: " + candidate);
        console.log("appMeta: " + appMeta);

        if (done) return;

        // Parse EEO meta data
        if (appMeta) {
          var eeoMeta = appMeta.split(',');
          if (eeoMeta.length != 4) return next(new Error('Failed to include all three EEO and Job Source fields'));
          jApp.eeo.gender = eeoMeta[0];
          jApp.eeo.race = eeoMeta[1];
          jApp.eeo.veteran = eeoMeta[2];
          jApp.jobSource = eeoMeta[3];
          jApp.appSource = "Comtech Careers";
        } else return next(new Error('Failed to include EEO and Job Source data fields'));
        jApp.candidateName = candidate.name.first + " " + candidate.name.last;

        // See if candidate's email address already exists in our database
        Cand.findOne({
          email: candidate.email
        })
        .populate('jobsAppliedTo', 'job')
        .exec(function(cfErr, user) {
            if (cfErr) return next(cfErr);

            if (!user) {
                ///////////////////////////////////////////////////////////////////////////////////////////
                // Candidate does not exists, create candidate, create application, associate cand to app
                ///////////////////////////////////////////////////////////////////////////////////////////
                console.log("Creating new Candidate: " + candidate.name);

                // candidate creation
                candidate.save(function(csErr) {
                    if (csErr) return next(csErr);

                    jApp.candidate = candidate._id;
                    // create job application
                    jApp.save(function(err2) {
                        if (err2) return next(err2);

                        // update candidate with job application id
                        candidate.jobsAppliedTo.push(jApp._id);
                        candidate.save(function(err3) {
                            if (err3) return next(err3);

                            // update job with applicationid
                            Job.findByIdAndUpdate(jApp.job, { $push: { jobApplications: jApp._id }}, function (err4, myjob) {
                                if (err4) return next(err4);

                                // Notify Admin that User has registered
                                var locals = {
                                   email: config.mailer.defaultFromAddress,
                                   subject: 'Job Application Received',
                                   name: {
                                    first: candidate.name.first,
                                    last: candidate.name.last
                                   },
                                   userEmail: candidate.email,
                                   jobTitle : jApp.jobTitle,
                                   toEmail: config.mailer.recruitingAddress,
                                   appUrl: config.host + '#!/applications/' + jApp._id
                                };

                                mailer.sendOne('job-application', locals, function (err5, responseStatus, html, text) {
                                    if (err5) return next(err5);

                                    res.render('comtechllc/success');
                                });       
                            });
                        });
                    });
                });
            } else {
                /////////////////////////////////////////////////////////////////////////////////////
                // Candidate already exists in the database, create application and update candidate
                /////////////////////////////////////////////////////////////////////////////////////
                var jobsApplied = _.pluck(user.jobsAppliedTo, 'job');
                if (_.findWhere(jobsApplied, jApp.job)) {
                    return res.render('comtechllc/success', {
                      title: 'Duplicate Application',
                      errorMsg: "You already applied to job: " + jApp.jobTitle
                    });
                } else {
                    console.log("Creating job application and updating candidate");
                    jApp.candidate = user._id;
                    // create job application
                    jApp.save(function(jsErr) {
                        if (jsErr) return next(jsErr);

                        // update candidate with new values provided
                        user.phone = candidate.phone;
                        user.name = {
                          first: candidate.name.first,
                          last: candidate.name.last
                        };
                        user.resume = candidate.resume;
                        // update candidate with job application id
                        user.jobsAppliedTo.push(jApp._id);
                        user.save(function(userErr) {
                            if (userErr) return next(userErr);

                            // update job with job applicationid
                            Job.findByIdAndUpdate(jApp.job, { $push: { jobApplications: jApp._id }}, function (err3, myjob) {
                                if (err3) return next(err3);
                                
                                // Notify Admin that User has registered
                                var locals = {
                                   email: config.mailer.defaultFromAddress,
                                   subject: 'Job Application Received',
                                   name: {
                                    first: candidate.name.first,
                                    last: candidate.name.last
                                   },
                                   userEmail: candidate.email,
                                   jobTitle : jApp.jobTitle,
                                   toEmail: config.mailer.recruitingAddress,                                   
                                   appUrl: config.host + '#!/applications/' + jApp._id
                                };

                                mailer.sendOne('job-application', locals, function (err5, responseStatus, html, text) {
                                    if (err5) return next(err5);

                                    res.render('comtechllc/success');
                                });
                            });
                        });
                    });
                }
            }
        });
    });

    form.parse(req);
};


/**
 * Create a job application using "Apply with LinkedIn"
exports.createLinkedin = function(p, q) {
    var application;
    var jApp = new JApp();
    console.log("Request the creation of an application object.");

    //application = JSON.parse(post_data);
    //console.log(req.body);
    application = p;
    application.job.id = q;
    application.job.id = new ObjectId(application.job.id);

    if (application.person) {
      Cand.findOne({
        email: application.person.emailAddress
      })
      .populate('jobsAppliedTo', 'job')
      .exec(function(err, user) {
          if (err) return next(err);
          if (!user) {
          ///////////////////////////////////////////////////////////////////////////////////////////
          // Candidate does not exists, create candidate, create application, associate cand to app
          ///////////////////////////////////////////////////////////////////////////////////////////
            var cand = new Cand();
            cand = linkedinToCand(cand, application);
            console.log("Creating linkedin Candidate: " + cand.name);

            // candidate creation
            cand.save(function(candErr) {
              if (candErr) {
                return res.send(400, { errors: candErr });
              } else {
                jApp.candidate = cand._id;
                jApp.candidateName = application.person.firstName + " " + application.person.lastName;
                jApp.job = application.job.id;
                jApp.jobTitle = application.job.position.title + " - " + application.job.locationDescription;
                // Parse EEO meta data
                if (application.meta) {
                  var eeoMeta = application.meta.split(',');
                  if (eeoMeta.length != 4) return res.send(400, {errors: 'Failed to include all 3 EEO meta data fields'});
                  jApp.eeo.gender = eeoMeta[0];
                  jApp.eeo.race = eeoMeta[1];
                  jApp.eeo.veteran = eeoMeta[2];
                  jApp.jobSource = eeoMeta[3];
                  jApp.appSource = "LinkedIn";
                }               
                // create job application
                jApp.save(function(err2) {
                  if (err2) {
                    return res.send(400, { errors: err2 });
                  } else {
                    // update candidate with job application id
                    cand.jobsAppliedTo.push(jApp._id);
                    cand.save(function(err3) {
                      if (err3) {
                        return res.send(400, { errors: err3 });
                      } else {
                        // update job with applicationid
                        Job.findByIdAndUpdate(application.job.id, { $push: { jobApplications: jApp._id }}, function (err4, myjob) {
                          if (err4) return res.send(400, {errors: err4});
                          else {
                            res.send(201);
                          }
                        });
                      }
                    });               
                  }
                });                
              }
            });
          } else {
            /////////////////////////////////////////////////////////////////////////////////////
            // Candidate already exists in the database, create application and update candidate
            /////////////////////////////////////////////////////////////////////////////////////
            var jobsApplied = _.pluck(user.jobsAppliedTo, 'job');
            if (_.findWhere(jobsApplied, application.job.id)) {
              var errorString = "Candidate: " + user._id + " already applied to job: " + application.job.id;
              console.log(errorString);
              return res.send(400, { errors: errorString });
            } else {
              console.log("Creating job application and updating linkedin candidate");
              jApp.candidate = user._id;
              jApp.candidateName = user.name.first + " " + user.name.last;
              jApp.job = application.job.id;
              jApp.jobTitle = application.job.position.title + " - " + application.job.locationDescription;
              // Parse EEO meta data
              if (application.meta) {
                var eeoMeta = application.meta.split(',');
                if (eeoMeta.length != 4) return res.send(400, {errors: 'Failed to include all 3 EEO meta data fields'});
                jApp.eeo.gender = eeoMeta[0];
                jApp.eeo.race = eeoMeta[1];
                jApp.eeo.veteran = eeoMeta[2];
                jApp.jobSource = eeoMeta[3];
                jApp.appSource = 'LinkedIn';
              }               
              jApp.save(function(jappErr) {
                if (jappErr) {
                  return res.send(400, { errors: jappErr });
                } else {
                  // update candidate with linkedin profile 
                  var updateCand = linkedinToCand(user, application);
                  // update candidate with job application id
                  updateCand.jobsAppliedTo.push(jApp._id);
                  updateCand.save(function(err2) {
                    if (err2) {
                      return res.send(400, { errors: err2 });
                    } else {
                      // update job with applicationid
                      Job.findByIdAndUpdate(application.job.id, { $push: { jobApplications: jApp._id }}, function (err3, myjob) {
                        if (err3) return res.send(400, {errors: err3});
                        else {
                          res.send(201);
                        }
                      });                    
                    }
                  });               
                }
              });
            }
          }
      });
    }
};*/

/////////////////////////////////////////////////////////////////////////////
// Utility function to convert a linkedin profile object to candidate object
/////////////////////////////////////////////////////////////////////////////
exports.linkedinToCand= function(cand, application) {

  exp = JSON.parse(application._raw);
  cand.name.first = application._json.firstName;
  cand.name.last = application._json.lastName;
  cand.email = application._json.emailAddress;
  cand.phone = application.phone;

    if (exp.threeCurrentPositions && exp.threeCurrentPositions.values) {
        cand.title = exp.threeCurrentPositions.values[0].title;
    }

  // Linkedin Recommendations
  if (exp.recommendationsReceived && exp.recommendationsReceived.values) {
    var recs = exp.recommendationsReceived.values;
    cand.recommendations = [];
    for (var i = 0; i < exp.recommendationsReceived.values.length; i++) {
      var myrec = {};
      myrec.recType = recs[i].recommendationType.code;
      myrec.recText = recs[i].recommendationText;
      myrec.recommender = recs[i].recommender.firstName + " " + recs[i].recommender.lastName;
      cand.recommendations.push(myrec);
    }
  }

  //Not available any more ?

  cand.linkedinProfile = exp.publicProfileUrl;
  // Location
  if (exp.location) {
    cand.location = {};
    cand.location.city = exp.location.name;
    if (exp.location.country) {
      if (exp.location.country.code == 'us') {
        cand.location.market = 'U.S.';
      } else {
      cand.location.market = exp.location.country.code;
      }
    } else {
      if (!cand.location.market) {
        cand.location.market = 'U.S.';
      }
    }

  }
  // Linkedin Positions
  if (exp.positions && exp.positions.values) {
    var pos = exp.positions.values;
    // reset value of positions array before pushing to it
    cand.positions = [];
    for (var j = 0; j < exp.positions.values.length; j++) {
      var mypos = {};
      mypos.title = pos[j].title;
      mypos.summary = pos[j].summary;
      mypos.startDate = {};
      if (pos[j].startDate) {
        if (pos[j].startDate.month) {
          mypos.startDate.month = pos[j].startDate.month;
        }
        if (pos[j].startDate.year) {
          mypos.startDate.year = pos[j].startDate.year;
        }                  
      }
      mypos.endDate = {};
      if (pos[j].endDate) {
        if (pos[j].endDate.month) {
          mypos.endDate.month = pos[j].endDate.month;
        }
        if (pos[j].endDate.year) {
          mypos.endDate.year = pos[j].endDate.year;
        }                  
      }
      mypos.isCurrent = pos[j].isCurrent;
      mypos.company = pos[j].company.name;
      cand.positions.push(mypos);               
    }
  }
  // Linkedin Skills
  if (exp.skills && exp.skills.values) {
    var skills = exp.skills.values;
    // reset the value of skills array before pushing
    cand.skills = [];
    for (var k=0; k < exp.skills.values.length; k++){
      cand.skills.push(skills[k].skill.name);
    }
  }
  // Linkedin Languages
  if (exp.languages && exp.languages.values){
    // reset languages array before pushing to it
    cand.languages = [];
    var langs = exp.languages.values;
    for (var l=0; l < exp.languages.values.length; l++) {
      cand.languages.push(langs[l].language.name);
    }
  }
  // Linkedin Certifications
  if (exp.certifications && exp.certifications.values) {
    cand.certifications = [];
    var certs = exp.certifications.values;
    for (var m=0; m < exp.certifications.values.length; m++) {
      cand.certifications.push(certs[m].name);
    }
  }
  // Linkedin Educations
  if (exp.educations && exp.educations.values) {
    var ed = exp.educations.values;
    cand.educations = [];
    for (var n=0; n < exp.educations.values.length; n++) {
      var myed = {};
      myed.fieldOfStudy = ed[n].fieldOfStudy;
      myed.degree = ed[n].degree;
      myed.schoolName = ed[n].schoolName;
      myed.activities = ed[n].activities;
      myed.startDate = {};
      if (ed[n].startDate) {
        if (ed[n].startDate.month) {
          myed.startDate.month = ed[n].startDate.month;
        }
        if (ed[n].startDate.year) {
          myed.startDate.year = ed[n].startDate.year;
        }
      }
      myed.endDate = {};
      if (ed[n].endDate) {
        if (ed[n].endDate.month) {
          myed.endDate.month = ed[n].endDate.month;
        }
        if (ed[n].endDate.year) {
          myed.endDate.year = ed[n].endDate.year;
        }
      }
      cand.educations.push(myed);
    }
  }
  // Add jobid if it's not already in the array
  //if (!_.contains(cand.jobsAppliedTo, application.job.id)) {
  //  cand.jobsAppliedTo.push(application.job.id);
  //}
  return cand;
};