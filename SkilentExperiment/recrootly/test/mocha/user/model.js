/**
 * Module dependencies.
 */
var should = require('should'),
    app = require('../../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User');

//Globals
var user;

//The tests
describe('<Unit Test>', function() {
    describe('Model User:', function() {
        before(function(done) {
            user = new User({
                name: {
                    first: 'Fist name',
                    last: 'Last name'
                },
                email: 'test@test.com',
                //username: 'user',
                password: 'password',
                provider: 'local',
                regStatus: 'new',
                updated: Date.now
            });
            user2 = new User({
                name: {
                    first: 'First name 2',
                    last: 'Last name 2'
                },
                email: 'test2@test.com',
                password: 'password2',
                provider: 'local',
                regStatus: 'new',
                updated: Date.now
            });

            done();
        });

        describe('Method Save', function() {
            it('should begin with no users', function(done) {
                User.find({}, function(err, users) {
                    users.should.have.length(0);
                    done();
                });
            });

            it('should be able to save whithout problems', function(done) {
                user.save(done);
            });

            it('should fail to save an existing user again', function(done) {
                user.save();
                return user2.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without name', function(done) {
                user.name = '';
                return user.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without email', function(done) {
                user.email = '';
                return user.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without registration Status', function(done) {
                user.regStatus = '';
                return user.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

        });

        after(function(done) {
            User.remove().exec();
            done();
        });
    });
});