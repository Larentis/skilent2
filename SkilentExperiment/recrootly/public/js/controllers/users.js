angular.module('mean.users').controller('UsersController', ['$scope', '$routeParams', '$location', 'Global', 'UserServices', 'UserSrv', 'RolesSrv', '_', '$window', function ($scope, $routeParams, $location, Global, UserServices, UserSrv, RolesSrv, _, $window) {
    $scope.global = Global;
    $scope.newRole = { name: '' };
    $scope.user = {};

    RolesSrv.getAll().success(function(data, status) {
        $scope.roles = data;
    })
        .error(function(err, status) {
            $scope.errorMessage = "Roles list not retrieved. " + err;
        });


    $scope.create = function() {
        console.log($scope.user.roles);

        UserServices.insertInternal($scope.user).success(function(res) {
            console.log('User ' + res._id + ' inserted into the database.');
            $location.path('users/' + res._id);
        }).error(function(err, status) {
            $window.scrollTo(0, 0);
            $scope.errorMessage = status + ": Error inserting user. " + JSON.stringify(err.errors);
        });

    };

    $scope.update = function() {
        UserServices.updateUser($scope.user).success(function(res) {
            console.log('User ' + res._id + ' updated in the database.');
            $location.path('users/' + res._id);
        }).error(function(err, status) {
            $window.scrollTo(0, 0);
            $scope.errorMessage = status + ": Error editing user. " + JSON.stringify(err.errors);
        });
    };

    $scope.approve = function(user) {
        $scope.errorMessage = '';

        if (user) {
            UserSrv.userApprove(user._id).success(function(response) {
                for (var myuser in $scope.userList) {
                    if ($scope.userList[myuser] == user) {
                        $scope.userList.splice(myuser, 1);
                    }
                }
                console.log("Approved User ID: " + response._id);
            })
                .error(function(err, status) {
                    $window.scrollTo(0, 0);
                    $scope.errorMessage = status + ": Error approving user. " + JSON.stringify(err.errors);

                });
        } else {
            $scope.errorMessage = "No user selected. ";

        }
    };

    $scope.addRole = function() {
        if ($scope.newRole.name) {
            var partsOfStr = $scope.newRole.name.split(',');
            for (var i in partsOfStr) {
                if (!$scope.user.roles) {
                    $scope.user.roles = [];
                }
                $scope.user.roles.push(partsOfStr[i]);
            }
            $scope.newRole.name = '';
        }
    };


    $scope.removeRole = function(role) {
        var i = $scope.user.roles.indexOf(role);
        if(i != -1) {
            $scope.user.roles.splice(i, 1);
        }
    };

    $scope.find = function() {
        UserServices.getAll().success(function(data, status) {
            $scope.users = data;
        })
            .error(function(err, status) {
                $scope.errorMessage = "Users list not retrieved. " + err;
            });
    };

    $scope.findSpecificUser = function() {
        UserServices.getUser($routeParams.id).success(function(data, status) {
            $scope.user = data;
        })
            .error(function(err, status) {
                $scope.errorMessage = err;
            });
    };

    $scope.userInfo = function(userId) {
        UserServices.getUser(userId).success(function(data, status) {
            $scope.user = data;
        })
            .error(function(err, status) {
                $scope.errorMessage = "Error retrieving this user.";
            });
    };

    $scope.findNew = function() {
        UserServices.getNewUsers().then(function(response) {
            $scope.userList = response;
        }, function(response) {
            $scope.errorMessage = "Error retrieving new users list.";
        });
    };

    $scope.findOneEdit = function() {
        JobsSrv.getJob($routeParams.jobId).success(function(data, status) {
            $scope.job = data;
            $scope.job.salary.public = $scope.job.salary.public.toString();
            $scope.job.leadRec = $scope.job.leadRec._id;
        });
    };

    $scope.findOne = function() {
        JobsSrv.getJob($routeParams.jobId).success(function(data, status) {
            $scope.job = data;
            $scope.job.salary.public = $scope.job.salary.public.toString();
        });
    };



    $scope.modalreject = function() {
        $scope.errorMessage = '';
        user = $scope.markForDeletion;
        if (user) {
            UserServices.remove(user).success(function(data, status) {
                for (var i in $scope.users) {
                    if ($scope.users[i] == user) {
                        $scope.users.splice(i, 1);
                    }
                }
            })
                .error(function(err, status) {
                    $scope.errorMessage = "Error removing user: " + status + ": " + err;
                });
        }
        else {
            console.log("Removing single user, not from list");
            Users.remove($scope.user._id, $scope.user);
            $location.path('users');
        }

    };

    $scope.selectUser = function(user) {
        $scope.markForDeletion = user;
    };


//////////////////////////////////////////////////////////////
// Filter Buttons
//////////////////////////////////////////////////////////////
    $scope.selectedStatus = _.pluck($scope.comtechJobStatus, 'code');

    $scope.setSelectedStatus = function (code) {
        console.log("My id: " + code);
        if (_.contains($scope.selectedStatus, code)) {
            $scope.selectedStatus = _.without($scope.selectedStatus, code);
        } else {
            $scope.selectedStatus.push(code);
        }
        return false;
    };

    $scope.isChecked = function (id) {
        if (_.contains($scope.selectedStatus, id)) {
            return 'glyphicon glyphicon-ok pull-right';
        }
        return false;
    };

    $scope.checkAll = function () {
        $scope.selectedStatus = _.pluck($scope.comtechJobStatus, 'code');
    };

    $scope.filterByStatus = function(job) {
        return ($scope.selectedStatus.indexOf(job.status) !== -1);
    };

    $scope.comtechMarkets = [
        {
            "name" : "Europe",
            "code" : "Europe"
        },
        {
            "name" : "India",
            "code" : "India"
        },
        {
            "name" : "Southeast Asia",
            "code" : "Southeast Asia"
        },
        {
            "name" : "U.S.",
            "code" : "U.S."
        }
    ];

 /*  $scope.roles = [
        {
            "name" : "Root",
            "code" : "Root"
        },
        {
            "name" : "Administrator",
            "code" : "Administrator"
        },
        {
            "name" : "Lead Internal Recruiter",
            "code" : "Lead Internal Recruiter"
        },
        {
            "name" : "Lead External Recruiter",
            "code" : "Lead External Recruiter"
        },
        {
            "name" : "Internal Recruiter",
            "code" : "Internal Recruiter"
        },
        {
            "name" : "External Recruiter",
            "code" : "External Recruiter"
        }
    ];*/


    $scope.states = [
        {
            "name" : "India",
            "abbreviation" : "India"
        },
        {
            "name": "Alabama",
            "abbreviation": "AL"
        },
        {
            "name": "Alaska",
            "abbreviation": "AK"
        },
        {
            "name": "American Samoa",
            "abbreviation": "AS"
        },
        {
            "name": "Arizona",
            "abbreviation": "AZ"
        },
        {
            "name": "Arkansas",
            "abbreviation": "AR"
        },
        {
            "name": "California",
            "abbreviation": "CA"
        },
        {
            "name": "Colorado",
            "abbreviation": "CO"
        },
        {
            "name": "Connecticut",
            "abbreviation": "CT"
        },
        {
            "name": "Delaware",
            "abbreviation": "DE"
        },
        {
            "name": "District Of Columbia",
            "abbreviation": "DC"
        },
        {
            "name": "Federated States Of Micronesia",
            "abbreviation": "FM"
        },
        {
            "name": "Florida",
            "abbreviation": "FL"
        },
        {
            "name": "Georgia",
            "abbreviation": "GA"
        },
        {
            "name": "Guam",
            "abbreviation": "GU"
        },
        {
            "name": "Hawaii",
            "abbreviation": "HI"
        },
        {
            "name": "Idaho",
            "abbreviation": "ID"
        },
        {
            "name": "Illinois",
            "abbreviation": "IL"
        },
        {
            "name": "Indiana",
            "abbreviation": "IN"
        },
        {
            "name": "Iowa",
            "abbreviation": "IA"
        },
        {
            "name": "Kansas",
            "abbreviation": "KS"
        },
        {
            "name": "Kentucky",
            "abbreviation": "KY"
        },
        {
            "name": "Louisiana",
            "abbreviation": "LA"
        },
        {
            "name": "Maine",
            "abbreviation": "ME"
        },
        {
            "name": "Marshall Islands",
            "abbreviation": "MH"
        },
        {
            "name": "Maryland",
            "abbreviation": "MD"
        },
        {
            "name": "Massachusetts",
            "abbreviation": "MA"
        },
        {
            "name": "Michigan",
            "abbreviation": "MI"
        },
        {
            "name": "Minnesota",
            "abbreviation": "MN"
        },
        {
            "name": "Mississippi",
            "abbreviation": "MS"
        },
        {
            "name": "Missouri",
            "abbreviation": "MO"
        },
        {
            "name": "Montana",
            "abbreviation": "MT"
        },
        {
            "name": "Nebraska",
            "abbreviation": "NE"
        },
        {
            "name": "Nevada",
            "abbreviation": "NV"
        },
        {
            "name": "New Hampshire",
            "abbreviation": "NH"
        },
        {
            "name": "New Jersey",
            "abbreviation": "NJ"
        },
        {
            "name": "New Mexico",
            "abbreviation": "NM"
        },
        {
            "name": "New York",
            "abbreviation": "NY"
        },
        {
            "name": "North Carolina",
            "abbreviation": "NC"
        },
        {
            "name": "North Dakota",
            "abbreviation": "ND"
        },
        {
            "name": "Northern Mariana Islands",
            "abbreviation": "MP"
        },
        {
            "name": "Ohio",
            "abbreviation": "OH"
        },
        {
            "name": "Oklahoma",
            "abbreviation": "OK"
        },
        {
            "name": "Oregon",
            "abbreviation": "OR"
        },
        {
            "name": "Palau",
            "abbreviation": "PW"
        },
        {
            "name": "Pennsylvania",
            "abbreviation": "PA"
        },
        {
            "name": "Puerto Rico",
            "abbreviation": "PR"
        },
        {
            "name": "Rhode Island",
            "abbreviation": "RI"
        },
        {
            "name": "South Carolina",
            "abbreviation": "SC"
        },
        {
            "name": "South Dakota",
            "abbreviation": "SD"
        },
        {
            "name": "Tennessee",
            "abbreviation": "TN"
        },
        {
            "name": "Texas",
            "abbreviation": "TX"
        },
        {
            "name": "Utah",
            "abbreviation": "UT"
        },
        {
            "name": "Vermont",
            "abbreviation": "VT"
        },
        {
            "name": "Virgin Islands",
            "abbreviation": "VI"
        },
        {
            "name": "Virginia",
            "abbreviation": "VA"
        },
        {
            "name": "Washington",
            "abbreviation": "WA"
        },
        {
            "name": "West Virginia",
            "abbreviation": "WV"
        },
        {
            "name": "Wisconsin",
            "abbreviation": "WI"
        },
        {
            "name": "Wyoming",
            "abbreviation": "WY"
        },
        {
            "name": "N/A",
            "abbreviation" : "N/A"
        }
    ];




}]);
