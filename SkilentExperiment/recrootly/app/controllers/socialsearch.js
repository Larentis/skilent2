exports.twittersearch = function (req, res) {

  var twitter = require('ntwitter-kanikar');

  var twit = new twitter({
    consumer_key: '9JMCCkMiOIXiJizeXJlIg',
    consumer_secret: 'tE7wzduMYONpPPi58v3JNzntoCzzuCrCmIjLAeMMQ0',
    access_token_key: '179990708-25Jz93qYnQGmtQbPotwO6dmMrg4feN7hQe6GbdKX',
    access_token_secret: 'RFTnbx16LGFJTr0KobAuolACZLj2LXiKALoMWrIKozEds'
  });

  if (typeof req.query.searchTerm == 'undefined' ||  req.query.searchTerm === '') {
    console.log("You didn't provide a searchTerm");
    res.json(false);
  }
  else {
      console.log("My query: " + req.query.searchTerm);
      console.log("Contacting Twitter...");
//    twit.search(req.query.searchTerm, {count:'10'}, function(err, data) {
      var mygeo = req.query.lat + "," + req.query.long + "," + req.query.radius;
      console.log("My geo is: " + mygeo);
      console.log("My searchTerm is: " + req.query.searchTerm); 
      twit.search(encodeURI(req.query.searchTerm), {count: '64', geocode: mygeo}, function(err, data) {
        if (err) {
            console.log(err);
            res.send(400, { errors: err });
        }
        else {
            console.log("Twitter Search success.");
            res.json(data);
        }
       });
   }      

};



exports.ghsearch = function (req, res) {

  var GitHubApi = require("github");
  var config = require('../../config/config'); 


  if (typeof req.query.searchTerm == 'undefined' ||  req.query.searchTerm === '') {
    console.log("You didn't provide a searchTerm");
    res.json(false);
  }
  else {

      var myLocation;
      if (req.query.location == 'undefined' || req.query.location === '') myLocation = '';
      else {
        myLocation = req.query.location.replace(/\s+/g, '');
        myLocation = myLocation.split(',')[0];
        console.log('MyLocation is: ' + myLocation);
      }
      var github = new GitHubApi({
          // required
          version: "3.0.0"
      });

      github.authenticate({
          type: "oauth",
          key: config.github.clientID,
          secret: config.github.clientSecret
      });

      github.search.users({
          // optional:
          // headers: {
          //     "cookie": "blahblah"
          // },
          //keyword: "keyword"
          q: ['language:'+ req.query.searchTerm, 'location:' + myLocation].join('+')
      }, function(err, myres) {
        if (err) {
          console.log(err);
          res.send(400, { errors: err });
        }
        else {
          res.json(myres);
        }
      });
   }      
};

exports.lisearch = function (req, res) {

  var Lin = require('linkedin-node');

  if (typeof req.query.searchTerm == 'undefined' ||  req.query.searchTerm === '') {
    console.log("You didn't provide a searchTerm");
    res.json(false);
  }
  else {
      var credentials = req.session.token;
      console.log("Contacting LinkedIn...");
      console.log("My searchTerm is: " + req.query.searchTerm); 
      console.log("My LinkedIn token: " + JSON.stringify(credentials));

      var api = Lin.api('v1', 'peopleAPI', 'search', {'keywords': req.query.searchTerm}); 
      Lin.makeRequest(credentials, {api:api}, function(err, data) { 
        if (err) {
            console.log(err);
            res.send(400, { errors: err });
        }
        else {
            console.log("LinkedIn Search success.");
            res.json(data);
        }
       });
   }      
};

exports.seTags = function (req, res) {

  var request = require('request');
  var config = require('../../config/config'); 
  var zlib = require('zlib');

  if (typeof req.query.searchTerm == 'undefined' ||  req.query.searchTerm === '') {
    console.log("You didn't provide a searchTerm");
    res.json(false);
  }
  else {
      console.log("My searchTerm is: " + req.query.searchTerm); 

      var reqUrl = config.stackexchange.path + 'tags/' + req.query.searchTerm + '/related';
      reqUrl = reqUrl + '?site=stackoverflow';
      reqUrl = reqUrl + '&key=' + config.stackexchange.key;
      console.log("Contacting StackExchange: " + reqUrl);

      var options = {
        url: reqUrl,
        headers: {
          'Accept-Encoding': 'gzip,deflate'
        }
      };
      //reqUrl = 'http://ziptasticapi.com/20147';
      //var myRes = request(options).pipe(zlib.createGunzip()).pipe(res);
      //gunzipJSON(myRes).pipe(res);

      var myReq = request(options);
     
      myReq.on('response', function (myres) {
        if (myres.statusCode !== 200) {
          //throw new Error('Status not 200');
          res.send(myres.statusCode, "Error retrieving StackExchange data");
        }
        else {
          var encoding = myres.headers['content-encoding'];
          if (encoding == 'gzip') {
            myres.pipe(zlib.createGunzip()).pipe(res);
          } else if (encoding == 'deflate') {
            myres.pipe(zlib.createInflate()).pipe(res);
          } else {
            myres.pipe(res);
        }
      }
      });
     
      req.on('error', function(err) {
        //throw err;
        res.send(400, err);
      });     
  }
         
};


exports.sfTags = function (req, res) {

  var request = require('request');
  var config = require('../../config/config'); 
  var zlib = require('zlib');

  if (typeof req.query.searchTerm == 'undefined' ||  req.query.searchTerm === '') {
    console.log("You didn't provide a searchTerm");
    res.json(false);
  }
  else {
      console.log("My serverFault tag searchTerm is: " + req.query.searchTerm); 

      var reqUrl = config.stackexchange.path + 'tags/' + req.query.searchTerm + '/related';
      reqUrl = reqUrl + '?site=serverfault';
      reqUrl = reqUrl + '&key=' + config.stackexchange.key;
      console.log("Contacting StackExchange: " + reqUrl);

      var options = {
        url: reqUrl,
        headers: {
          'Accept-Encoding': 'gzip,deflate'
        }
      };

      var myReq = request(options);
     
      myReq.on('response', function (myres) {
        if (myres.statusCode !== 200) {
          //throw new Error('Status not 200');
          res.send(myres.statusCode, "Error retrieving ServerFault tag data");
        }
        else {
          var encoding = myres.headers['content-encoding'];
          if (encoding == 'gzip') {
            myres.pipe(zlib.createGunzip()).pipe(res);
          } else if (encoding == 'deflate') {
            myres.pipe(zlib.createInflate()).pipe(res);
          } else {
            myres.pipe(res);
        }
      }
      });
     
      req.on('error', function(err) {
        //throw err;
        res.send(400, err);
      });     
  }
         
};


exports.seTopAnswerers = function (req, res) {

  var request = require('request');
  var config = require('../../config/config'); 
  var zlib = require('zlib');

  if (typeof req.query.searchTerm == 'undefined' ||  req.query.searchTerm === '') {
    console.log("You didn't provide a searchTerm");
    res.json(false);
  }
  else {
      var reqUrl = config.stackexchange.path + 'tags/' + req.query.searchTerm + '/top-answerers/all_time';
      reqUrl = reqUrl + '?site=stackoverflow';
      reqUrl = reqUrl + '&key=' + config.stackexchange.key;
      console.log("Contacting StackExchange Top Tag Answerers: " + reqUrl);

      var options = {
        url: reqUrl,
        headers: {
          'Accept-Encoding': 'gzip,deflate'
        }
      };

      var myReq = request(options);
     
      myReq.on('response', function (myres) {
        if (myres.statusCode !== 200) {        
          res.statusCode = myres.statusCode;
          //myres.pipe(res);
          //res.send(myres.statusCode, JSON.stringify(myres));          
          //res.send(myres.statusCode, "Error retrieving StackExchange Top Tag Answerers data");
          //console.log(JSON.stringify(myres));
        }
//        else {
          var encoding = myres.headers['content-encoding'];
          if (encoding == 'gzip') {
            myres.pipe(zlib.createGunzip()).pipe(res);
          } else if (encoding == 'deflate') {
            myres.pipe(zlib.createInflate()).pipe(res);
          } else {
            myres.pipe(res);
//        }
      }
      });
     
      myReq.on('error', function(err) {
        //throw err;
        res.send(400, err);
        console.log('Error retrieving StachExchange top answerers');
      });     
  }
};       

// exports.test = function(req, res) {
//   var request = require('request');  
//   //request('http://api.stackexchange.com/2.1/tags/javascript/related?site=stackoverflow').pipe(res);

//   request('http://api.stackexchange.com/2.1/tags/javascript/related?site=stackoverflow', function (error, response, body) {
//   if (!error && response.statusCode == 200) {
//     response.pipe(res);
//     console.log(body); // Print the google web page.
//   }
//   else {
//     res.json(400);
//   }
// });
// };         
