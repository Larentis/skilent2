//User service used for auth REST endpoint
angular.module('mean.users').factory('UserServices', ['$http', function($http) {

	var _injected = {},
		_user = {};
	var urlBase = '/users';
	var UserServices = {};

	function _clearUser() {
		_.each(_user || {}, function(v, k) {
			delete _user[k];
		});
	}

	function _setUser(userJSON) {
		_clearUser();
		_.assign(_user, userJSON);
	}

	UserServices.insertUser = function(user) {
		console.log('Hi');
		var fd = new FormData();
		fd.append("user", angular.toJson(user));


		return $http.post('/users', fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		});


	};

	UserServices.insert = function(user) {
		return $http.post('/api/v1/users', angular.toJson(user));
	};

	UserServices.insertInternal = function(user) {
		return $http.post('/api/v1/users/internal', angular.toJson(user));
	};

	UserServices.remove = function(user) {
		return $http.delete(urlBase + '/' + user._id, angular.toJson(user));
	};

	UserServices.updateUser = function(user) {
		var temp = urlBase + '/' + user._id;
		return $http.put(temp, angular.toJson(user));
	};

	UserServices.getUser = function(id) {
		return $http.get('/api/v1' + urlBase + '/' + id);
	};

	UserServices.getUsers = function() {
		return $http.get('/api/v1' + urlBase).then(function(result) {
			return result.data;
		});
	};

	UserServices.getAll = function() {
		return $http.get('/api/v1' + urlBase);
	};

	UserServices.getNewUsers = function() {
		return $http.get('/api/v1' + urlBase + '/new').then(function(result) {
			return result.data;
		});
	};

	UserServices.signin = function(formData) {
		return $http.post(urlBase + '/session', formData);
	};

	return UserServices;

}]);




//User factory service used for REST endpoint
angular.module('mean.jobs').factory('UserSrv', ['$http', '$q', function($http, $q) {

	var urlBase = '/api/v1/users';
	var Users = {};

	Users.userApprove = function(id) {
		return $http.put(urlBase + '/' + id + '/regStatus/approve');
	};

	Users.userReject = function(id) {
		return $http.put(urlBase + '/' + id + '/regStatus/reject');
	};

	return Users;

}]);