(function () {
    'use strict';

// Candidates Controller Spec

    describe('Skilent controllers', function () {

        describe('CandidatesController', function () {

            // The $resource service augments the response object with methods for updating and deleting the resource.
            // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
            // the responses exactly. To solve the problem, we use a newly-defined toEqualData Jasmine matcher.
            // When the toEqualData matcher compares two objects, it takes only object properties into
            // account and ignores methods.
            beforeEach(function () {
                jasmine.addMatchers({
                    toEqualData: function () { 
                        return {
                            compare: function(actual, expected) {
                                return {
                                    pass: angular.equals(actual, expected)
                                };
                            }
                        };
                    }
                });
            });

            // Load the controllers module
            beforeEach(module('mean'));
            beforeEach(module('mean.system'));
            beforeEach(module('mean.candidates'));

            // Initialize the controller and a mock scope
            var CandidatesController,
                scope,
                $httpBackend,
                $routeParams,
                $location;

            // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
            // This allows us to inject a service but then attach it to a variable
            // with the same name as the service.
            beforeEach(inject(function ($controller, $rootScope, _$location_, _$routeParams_, _$httpBackend_) {

                scope = $rootScope.$new();

                CandidatesController = $controller('CandidatesController', {
                    $scope: scope
                });

                $routeParams = _$routeParams_;

                $httpBackend = _$httpBackend_;

                $location = _$location_;


            }));

            it('$scope.find() should create an array with at least one candidate object ' +
                'fetched from XHR', function () {

                // test expected GET request
                $httpBackend.expectGET('/api/v1/candidates').respond([{
                    title: 'Fantastic Dev', 
                    name: {first: 'Super', last: 'Awesome'}
                }]);

                // run controller
                scope.find();
                $httpBackend.flush();

                // test scope value
                expect(scope.candidates).toEqualData([{
                    title: 'Fantastic Dev', 
                    name: {first: 'Super', last: 'Awesome'}
                }]);

            });

            it('$scope.findOne() should create an array with one candidate object fetched ' +
                'from XHR using a candidateId URL parameter', function () {
                // fixture URL parament
                $routeParams.candidateId = '525a8422f6d0f87f0e407a33';

                // fixture response object
                var testCandidateData = function () {
                    return {
                        title: 'Fantastic Dev',
                        name : {
                            first: 'Super',
                            last: 'Awesome'
                        }
                    };
                };

                // test expected GET request with response object
                $httpBackend.expectGET(/api\/v1\/candidates\/([0-9a-fA-F]{24})$/).respond(testCandidateData());

                // run controller
                scope.findOne();
                $httpBackend.flush();

                // test scope value
                expect(scope.candidate).toEqualData(testCandidateData());

            });

            // it('$scope.create() with valid form data should send a POST request ' +
            //     'with the form input values and then ' +
            //     'locate to new object URL', function () {

            //     // fixture expected POST data
            //     var postCandidateData = function () {
            //         return {
            //             title: 'Fantastic Dev',
            //             name : {
            //                 first: 'Super',
            //                 last: 'Awesome'
            //             },
            //             salary: {
            //                 current: '5000',
            //                 desired: '100000'
            //             }
            //         };
            //     };


            //     // fixture expected response data
            //     var responseCandidateData = function () {
            //         return {
            //             _id: '525cf20451979dea2c000001',
            //             title: 'Fantastic Dev',
            //             name : {
            //                 first: 'Super',
            //                 last: 'Awesome'
            //             },
            //             salary: {
            //                 current: '5000',
            //                 desired: '100000'
            //             }                       
            //         };
            //     };

            //     // fixture mock form input values
            //     var cand = {};
            //     var myname = {};
            //     myname = {first: "Super", last: "Awesome"};
            //     cand.name = myname;
            //     var salary = {};
            //     salary = {current: '5000', desired: '100000' };
            //     cand.salary = salary;
            //     cand.title = 'Fantastic Dev';
            //     cand._id = '525cf20451979dea2c000001';
  
            //     scope.cand = cand;
            //     scope.myFile = '';
            //     scope.skills = ['Java', 'HTML'];

            //     // fixture expected response data
            //     var fd = new window.FormData();
            //     for (var i in scope.cand){
            //         if (i !== 'name' && i !== 'salary')
            //         {
            //             fd.append(i,cand[i]);               
            //         }
            //     }                
            //     fd.append('nameFirst', scope.cand.name.first);
            //     fd.append('nameLast', scope.cand.name.last);
            //     fd.append('salaryCurrent', scope.cand.salary.current);
            //     fd.append('salaryDesired', scope.cand.salary.desired);
            //     fd.append('file', '');               

            //     // test post request is sent
            //     $httpBackend.expectPOST('/api/v1/candidates', cand).respond({});

            //     // Run controller
            //     scope.create();
            //     $httpBackend.flush();

            //     // test form input(s) are reset
            //     expect(scope.cand.title).toEqual('');
            //     //expect(scope.cand.name.first).toEqual('');
            //     //expect(scope.cand.name.last).toEqual('');

            //     // test URL location to new object
            //     expect($location.path()).toBe('/candidates/' + responseCandidateData()._id);
            // });

            it('$scope.update() should update a valid candidate', inject(function (Candidates) {


                // fixture rideshare
                var putCandidateData = function () {
                    return {
                        _id: '525a8422f6d0f87f0e407a33',
                        title: 'Fantastic Dev',
                        name: {
                            first: 'Super',
                            last: 'Awesome'
                        },
                        salary: {
                            current: '5000',
                            desired: '100000'
                        }
                    };
                };

                var candidate = {};
                // mock candidate object from form
                candidate = putCandidateData;

                // mock candidate in scope
                scope.candidate = candidate;

                // fixture URL parament
                $routeParams.candidateId = '525a8422f6d0f87f0e407a33';                

                // test PUT happens correctly
                $httpBackend.expectPUT(/api\/v1\/candidates\/([0-9a-fA-F]{24})$/).respond();

                // testing the body data is out for now until an idea for testing the dynamic updated array value is figured out
                //$httpBackend.expectPUT(/candidates\/([0-9a-fA-F]{24})$/, putCandidateData()).respond();
                /*
                Error: Expected PUT /candidates\/([0-9a-fA-F]{24})$/ with different data
                EXPECTED: {"_id":"525a8422f6d0f87f0e407a33","title":"Fantastic Dev","name.first":"Super", "name.last":"Awesome"}
                GOT:      {"_id":"525a8422f6d0f87f0e407a33","title":"Fantastic Dev","name.first":"Super", "name.last":"Awesome", "updated":[1383534772975]}
                */

                // run controller
                scope.update();
                $httpBackend.flush();

                // test URL location to new object
                expect($location.path()).toBe('/candidates/' + putCandidateData()._id);

            }));

            it('$scope.modalremove() should send a DELETE request with a valid candidateId' +
                'and remove the candidate from the scope', inject(function (Candidates) {

                // fixture rideshare
                var candidate = {
                    _id: '525a8422f6d0f87f0e407a33'
                };


                // mock rideshares in scope
                //scope.candidates = [];
                //scope.candidates.push(candidate);
                scope.selectCandidate(candidate);
                scope.candidates = [candidate];

                // test expected rideshare DELETE request
                $httpBackend.expectDELETE(/candidates\/([0-9a-fA-F]{24})$/).respond(204);

                // run controller
                scope.modalremove(candidate);
                $httpBackend.flush();



                // test after successful delete URL location candidates list
                //expect($location.path()).toBe('/candidates');
                expect(scope.candidates.length).toBe(0);

            }));

        });

    });
}());

