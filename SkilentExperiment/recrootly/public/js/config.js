//Setting up route
angular.module('mean').config(['$routeProvider',
    function($routeProvider) {

        //================================================
        // Check if the user is connected
        //================================================
        var checkLoggedin = function($q, $timeout, $http, $location, $rootScope){
            // Initialize a new promise
            var deferred = $q.defer();

            // Make an AJAX call to check if the user is logged in
            $http.get('/loggedin').success(function(user){
                // Authenticated
                if (user !== '0')
                /*$timeout(deferred.resolve, 0);*/
                    deferred.resolve();

                // Not Authenticated
                else {
                    $rootScope.message = 'You need to log in.';
                    //$timeout(function(){deferred.reject();}, 0);
                    deferred.reject();
                    $location.url('/');
                }
            });

            return deferred.promise;
        };
        //================================================

        $routeProvider.
            when('/candidates', {
                templateUrl: 'views/candidates/list.html'
            }).
            when('/candidates/create', {
                templateUrl: 'views/candidates/create.html'
            }).
            when('/candidates/:candidateId/edit', {
                templateUrl: 'views/candidates/edit.html'
            }).
            when('/candidates/:candidateId', {
                templateUrl: 'views/candidates/view.html'
            }).
            when('/jobs', {
                templateUrl: 'views/jobs/list.html'
            }).
            when('/jobs/create', {
                templateUrl: 'views/jobs/create.html'
            }).
            when('/jobs/:jobId/edit', {
                templateUrl: 'views/jobs/edit.html'
            }).
            when('/jobs/:jobId', {
                templateUrl: 'views/jobs/view.html'
            }).
            when('/applications', {
                templateUrl: 'views/applications/list.html'
            }).
            when('/applications/:applicationId', {
                templateUrl: 'views/applications/view.html'
            }).
            when('/applications/:applicationId/edit', {
                templateUrl: 'views/applications/edit.html'
            }).
            when('/', {
                templateUrl: 'views/index.html'
            }).
            when('/socialsearch', {
                templateUrl: 'views/socialsearch.html',
                resolve: {
                    loggedin: checkLoggedin
                }
            }).
            when('/admin123', {
                templateUrl: 'views/users/openlist.html'
            }).
            when('/settings', {
                templateUrl: 'views/settings/list.html'
            }).
            when('/settings/create', {
                templateUrl: 'views/settings/create.html'
            }).
            when('/settings/:id/edit', {
                templateUrl: 'views/settings/edit.html'
            }).
            when('/settings/:id', {
                templateUrl: 'views/settings/view.html'
            }).
            when('/users/:id', {
                templateUrl: 'views/settings/view.html'
            }).
            when('/roles', {
                templateUrl: 'views/roles/list.html'
            }).
            when('/roles/create', {
                templateUrl: 'views/roles/create.html'
            }).
            when('/roles/:id', {
                templateUrl: 'views/roles/view.html'
            }).
            when('/roles/:id/edit', {
                templateUrl: 'views/roles/edit.html'
            }).
            when('/email/:candidateId', {
                templateUrl: 'views/candidates/email.html'
            }).
            otherwise({
                redirectTo: '/'
            });
    }
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix("!");
    }
]);



// Setting up Interceptors
angular.module('mean').config(['$httpProvider',
    function($httpProvider) {
        // http interceptors for Unauthorized access
        $httpProvider.interceptors.push(['$q', '$location', function($q, $location) {

            return {
                response: function(response) {
                    // response.status === 200
                    return response || $q.when(response);
                },
                responseError: function(rejection) {
                    // Executed only when the XHR response has an error status code

                    if (rejection.status == 401) {

                        // The interceptor "blocks" the error;
                        // and the success callback will be executed.

                        rejection.data = { status: 401, description: 'unauthorized' };
                        console.log("Rejection data: " + JSON.stringify(rejection.data));
                        $location.path("/");
                    }

                    /*
                     $q.reject creates a promise that is resolved as
                     rejected with the specified reason.
                     In this case the error callback will be executed.
                     */

                    return $q.reject(rejection);
                }
            };

        }]);
    }
]);