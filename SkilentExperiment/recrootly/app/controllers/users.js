/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    config = require('../../config/config'),
    _ = require('underscore'),
    mailer = require('./mailer');

// /**
//  * Auth callback
//  */
// exports.authCallback = function(req, res, next) {
//     res.redirect('/signin/success');
// };

/**
 * Show login form
 */
exports.signin = function(req, res) {
    res.render('users/signin', {
        title: 'Signin',
        message: req.flash('error')
    });
};


/**
 * Show sign up form
 */
exports.register = function(req, res) {
    res.render('users/register', {
        title: 'Sign up',
        user: new User()
    });
};

/**
 * Logout
 */
exports.signout = function(req, res) {
    req.logout();
    res.redirect('/');
};

/**
 * Show sign up form
 */
exports.confirmation = function(req, res) {
    res.render('users/confirmation', {
        title: 'Registration - Admin Review'
    });
};

/**
 * Session
 */
// exports.session = function(req, res) {
//     switch (req.user.regStatus) {
//         case "Approved":
//             res.redirect('/signin/sucess'); break;
//         case "New":
//             res.redirect('/usersconfirmation'); break;
//         case "Rejected":
//             res.redirect('/signin'); break;
//     }
// };


/**
 * Reset background variable
 */
exports.success = function(req, res) {
    res.locals.mypath = "hp";
    res.redirect('/#!/candidates');
};

/**
 * Create user
 */
exports.createI = function(req, res) {
    var user = new User(req.body);

    console.log("My user name: " + user.name.first );
    console.log("My user last: " + user.name.last );
    console.log("My email: " + user.email );
    console.log("My password: " + user.password );

    user.provider = 'local';
    user.regStatus = 'New';
    user.roles = ['Default'];

    user.save(function(err) {
        if (!err) {
            console.log("User registered.");
            // Notify Admin that User has registered
            var locals = {
                email: config.mailer.defaultFromAddress,
                subject: 'User Registered',
                name: {
                    first: user.name.first,
                    last: user.name.last
                },
                userEmail: user.email,
                provider : user.provider,
                activateUrl: config.host + '#!/admin123/'
            };

            mailer.sendOne('registration', locals, function (err2, responseStatus, html, text) {
                if (err2) {
                    console.log("Error sending email to admin: " + err2);
                    return res.render('/register', {
                        errors: err,
                        user: user
                    });
                } else {
                    console.log("Response Status: " + responseStatus);
                    return res.redirect('/regConf');
                }
            });
            // req.logIn(user, function(err) {
            //     if (err) return next(err);
            //         return res.redirect('/');
            // });
        } else {
            console.log("User registration errors: " + err);
            return res.render('/register', {
                errors: err,
                user: user
            });
        }
    });
};

/**
 * Create user internally
 */
exports.create = function(req, res) {
    var user = new User(req.body);
    console.log(req.body);

    user.provider = 'local';
    user.regStatus = 'Approved';
    user.password = 'comtechllc';
    user.roles = ['Default'];

    console.log("My user name: " + user.name.first );
    console.log("My user last: " + user.name.last );
    console.log("My email: " + user.email );
    console.log("My password: " + user.password );
    console.log("My id: " + user._id);

    user.save(function(err) {
        if (!err) {
            console.log("User registered.");
            res.redirect('/api/v1/users/' + user._id);

        } else {
            console.log("User registration errors: " + err);
        }
    });


};


/**
 * Edit and update a user profile on file, not the current 'user' logged into the application
 */
exports.updateProfile = function(req, res) {

    var user = req.profile;
    console.log(req.profile.id);
    // clone object using underscore util
    user = _.extend(user, req.body);

    user.save(function(err) {
        if (err) {
            return res.send(400, { errors: err });
        } else {
            console.log('Updated.');
            return res.json(req.profile || null);
        }
    });
};

/**
 *  Show profile
 */
exports.show = function(req, res) {
    var user = req.profile;

    res.render('users/show', {
        title: user.name,
        user: user
    });
};
/**
 *
 * Send a specific user profile, not current logged in user
 */
exports.showUser = function(req, res) {
    res.json(req.profile || null);
    console.log('Here is the profile you want !');
    //console.log(req);
    //console.log(req.profile);
};

/**
 * Delete a user
 */
exports.destroy = function(req, res) {
    var user = req.profile;

    user.remove(function(err) {
        if (err) {
            console.log("My error: " + err);
            res.send(500, { errors: err });
        } else {
            res.jsonp(user);
            res.redirect('/settings/' + user._id);
        }
    });
};

/**
 * Send User
 */
exports.me = function(req, res) {
    res.jsonp(req.user || null);
};

/**
 * Find user by id
 */
exports.user = function(req, res, next, id) {
    User
        .findOne({
            _id: id
        })
        .exec(function(err, user) {
            if (err) return next(err);
            if (!user) return next(new Error('Failed to load User ' + id));
            req.profile = user;
            next();
        });
};


// Update a person
exports.approve = function (req, res, next){
    return User.findById(req.params.id, function (err, u) {
        u.regStatus = "Approved";
        u.updated =  Date.now();
        return u.save(function (err2) {
            if (!err2) {
                console.log("User regStatus Approved, ID: " + u._id);

                // Notify User that account has been approved
                var locals = {
                    email: config.mailer.defaultFromAddress,
                    subject: 'User Account Approved',
                    name: {
                        first: u.name.first,
                        last: u.name.last
                    },
                    userEmail: u.email,
                    provider : u.provider,
                    signInUrl: config.host
                };

                mailer.sendOne('account-approval', locals, function (err3, responseStatus, html, text) {
                    if (err) {
                        console.log("Error sending email to user: " + err3);
                        return res.render('/#!/admin123', {
                            errors: err
                        });
                    } else {
                        console.log("User approval email sent");
                        return res.jsonp(u);
                    }
                });

            } else {
                console.log("Error approving user ID " + u._id + ": " + err2);
                return res.send(next(err2));
            }
        });
    });
};


// Update a person
exports.reject = function (req, res, next){
    return User.findById(req.params.id, function (err, u) {
        u.RegStatus = "Rejected";
        u.updated =  Date.now();
        return u.save(function (err2) {
            if (!err2) {
                console.log("User regStatus Rejected.");
                return res.jsonp(u);
            } else {
                console.log(err2);
                return res.send(next(err2));
            }
        });
    });
};


/**
 * List of Users
 */
exports.all = function(req, res) {
    User.find().sort('-created').select('name email provider roles regStatus').exec(function(err, users) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(users);
        }
    });
};


/**
 * List of Users with status 'New'
 */
exports.allnew = function(req, res) {
    User.find({ 'regStatus': 'New' }).sort('-created').select('_id name email provider regStatus created').exec(function(err, users) {
        if (err) {
            return res.send(500, { errors: err.errors });
        }
        if (!users) {
            return res.send(404, { errors: err.errors });
        }
        res.jsonp(users);
    });
};