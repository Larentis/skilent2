//Roles factory service used for REST endpoint
angular.module('mean.roles').factory('RolesSrv', ['$http', '$q', function($http, $q) {

    var urlBase = '/api/v1/roles';
    var RolesSrv = {};

    RolesSrv.insertRole = function(role) {
        return $http.post(urlBase, role);
    };

    RolesSrv.updateRole = function(role) {
        var temp = urlBase + '/' + role._id;
        return $http.put(temp, angular.toJson(role));
    };

    RolesSrv.getAll = function() {
        return $http.get(urlBase);
    };

    RolesSrv.getRole = function(id) {
        return $http.get(urlBase + '/' + id);
    };

    return RolesSrv;

}]);

