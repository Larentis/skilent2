/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    Schema = mongoose.Schema;

/**
 * Job Schema
 */
var JobSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    dateOpened: { type: Date, default: Date.now, required: true},
    updated : { type: Date, default: Date.now, required:true },
    updatedBy: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    company: { type: String, default: 'Comtech', required:true },
    title: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
    opNum: {type: String},
    jobCode : { type: String},
    projName : { type: String, required:true },
    replacement: { type: String, required: true },
    replacementName: { type: String },
    location : { 
        city: {type: String, required: true },
        state: {type: String, required: true},
        market: {type: String}
    },
    startDate : { type: Date },
    minExp : { type: Number, required: true },
    minEducation : { type: String, required: true },
    minEducationOther : { type: String },
    minEducationPref : { type: String, required: true },
    minEducationPrefOther : { type: String },
    certReq : { type: String },
    certPref : { type: String },
    qualifications: {
        type: String,
        default: '',
        trim: true,
        required : true
    },
    responsibilities: {
        type: String,
        default: '',
        trim: true,
        required : true
    },
    functions: {
        type: String,
        default: '',
        trim: true,
        required: true
    },
    workAuth: {type : String, required: true },
    workAuthOther: {type : String },
    jobType: { type: String, required: true},
    clearanceReq: { type: String },
    salary: {
        annualRange : { type: String},
        hourlyRange : { type: String},
        public : { type: Boolean}
    },
    billRate : { type: String },
    signBonus : { type: String},
    relo: { type: String },
    telework : {type : Boolean},
    skills: [{ type : String }],
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    hiringMgr: {
        first: { type: String, required: true },
        last: { type: String, required: true }
    },  
    status: { type : String, default: 'Open - Proposal' },
    category : { type: String },
    leadRec : { type: Schema.ObjectId, ref : 'User'},
    closedDate : { type: Date },
    candidateHired : { type: Schema.ObjectId, ref: 'Candidate'},
    jobApplications : [{
        type: Schema.ObjectId,
        ref: 'Application'
    }]
});

/**
 * Validations
 */
JobSchema.path('title').validate(function(title) {
    return title.length;
}, 'Title cannot be blank');


/**
 * Statics
 */
JobSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name email').populate('updatedBy', 'name email').populate('leadRec', '_id name').populate('jobApplications', 'job candidate candidateName status created').exec(cb);
};


mongoose.model('Job', JobSchema);