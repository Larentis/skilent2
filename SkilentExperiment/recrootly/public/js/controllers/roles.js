angular.module('mean.roles').controller('RolesController', ['$scope', '$routeParams', '$location', 'Global', 'UserServices', 'UserSrv', 'RolesSrv', '_', '$window', function ($scope, $routeParams, $location, Global, UserServices, UserSrv, RolesSrv, _, $window) {
    $scope.global = Global;
    $scope.role = {};


$scope.findRoles = function() {
    RolesSrv.getAll().success(function(data, status) {
        $scope.roles = data;
    })
        .error(function(err, status) {
            $scope.errorMessage = "Roles list not retrieved. " + err;
        });
};

$scope.createRole = function() {
    RolesSrv.insertRole($scope.role).success(function(res) {
        console.log('Role ' + res._id + ' inserted into the database.');
        console.log(res);
        $location.path('roles/' + res._id);
    }).error(function(err, status) {
        $window.scrollTo(0, 0);
        $scope.errorMessage = status + ": Error inserting role. " + JSON.stringify(err.errors);
    });

};

$scope.findRole = function() {
     RolesSrv.getRole($routeParams.id).success(function(data, status) {
          $scope.role = data;
     }).error(function(err,status) {
         $scope.errorMessage = 'Individual role not retrieved.' + err;
     });
};

$scope.updateRole = function() {
    var role = $scope.role;


    $scope.errorMessage = '';

    RolesSrv.updateRole(role).success(function(response) {
        console.log("Updated role with ID: " + $routeParams.id);
        $location.path('roles/' + $routeParams.id);
    })
        .error(function(err, status) {
            console.log("Error updating role: " + status + ": " + err );
            $scope.errorMessage = "Error updating role: " + status + ": " + err;
        });
};



}]);


