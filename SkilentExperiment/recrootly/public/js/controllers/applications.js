angular.module('mean.applications').controller('ApplicationsController', ['$scope', '$routeParams', '$location', 'Global', 'Applications', '$window', function ($scope, $routeParams, $location, Global, Applications, $window) {
    $scope.global = Global;
    $scope.skills = [];

    $scope.find = function() {
        Applications.getAll().success(function(data, status) {
            $scope.applications = data;
        })
        .error(function(err, status) {
            $scope.errorMessage = "Job applications list not retrieved. " + err;
        });
    };

    $scope.findOne = function() {    
        $scope.applicationId = $routeParams.applicationId;
        Applications.getApp($routeParams.applicationId).success(function(data, status) {
            $scope.application = data;
        });
    };

    $scope.update = function() {
        $scope.errorMessage = '';
        $scope.successMessage = '';
        var application = $scope.application;

        Applications.updateApp(application).success(function(response) {
            //$scope.successMessage = "Application saved.";
            $location.path('applications/' + $routeParams.applicationId);
        })
        .error(function(err, status) {
            console.log("Error updating application: " + status + ": " + err );
            $scope.Message = "Error updating application: " + status + ": " + err;
        });
    };


//////////////////////////////////////////////////////////////
// Status Filter Buttons
//////////////////////////////////////////////////////////////
    $scope.selectedStatus = _.pluck($scope.comtechAppStatus, 'code');

    $scope.setSelectedStatus = function (code) {
        if (_.contains($scope.selectedStatus, code)) {
            $scope.selectedStatus = _.without($scope.selectedStatus, code);
        } else {
            $scope.selectedStatus.push(code);
        }
        return false;
    };

    $scope.isChecked = function (id) {
        if (_.contains($scope.selectedStatus, id)) {
            return 'glyphicon glyphicon-ok pull-right';
        }
        return false;
    };

    $scope.checkAll = function () {
        $scope.selectedStatus = _.pluck($scope.comtechAppStatus, 'code');
        //console.log($scope.selectedStatus);
    };

    $scope.filterByStatus = function(job) {
        return ($scope.selectedStatus.indexOf(job.status) !== -1);
    };


//////////////////////////////////////////////////////////////
// Static Values -- To be Created as Service [in the future]
//////////////////////////////////////////////////////////////

    $scope.comtechAppStatus = [
    {
        "name" : "Filed",
        "code" : "Filed"
    },
    {
        "name" : "Tech Screen",
        "code" : "Tech Screen"
    },
    {
        "name" : "Client Screen",
        "code" : "Client Screen"
    },
    {
        "name" : "Rejected",
        "code" : "Rejected"
    },
    {
        "name" : "Hire",
        "code" : "Hire"
    }             
    ]; 

    $scope.eeoGender = [
    {
        "name" : "Female",
        "code" : "Female"
    },
    {
        "name" : "Male",
        "code" : "Male"
    },
    {
        "name" : "Choose Not to Disclose",
        "code" : "Choose Not to Disclose"
    }            
    ];  

    $scope.eeoRace = [
    {
        "name" : "Hispanic or Latino",
        "code" : "Hispanic or Latino"
    },
    {
        "name" : "White (not Hispanic or Latino)",
        "code" : "White (not Hispanic or Latino)"
    },
    {
        "name" : "Black or African American (not Hispanic or Latino)",
        "code" : "Black or African American (not Hispanic or Latino)"
    },
    {
        "name" : "Native Hawaiian or Other Pacific Islander (not Hispanic or Latino)",
        "code" : "Native Hawaiian or Other Pacific Islander (not Hispanic or Latino)"
    },
    {
        "name" : "Asian (not Hispanic or Latino)",
        "code" : "Asian (not Hispanic or Latino)"
    },
    {
        "name" : "American Indian or Alaska Native (not Hispanic or Latino)",
        "code" : "American Indian or Alaska Native (not Hispanic or Latino)"
    },
    {
        "name" : "Two or More Races (not Hispanic or Latino)",
        "code" : "Two or More Races (not Hispanic or Latino)"
    },
    {
        "name" : "Choose Not to Disclose",
        "code" : "Choose Not to Disclose"
    }                                 
    ]; 

    $scope.eeoVeteran = [
    {
        "name" : "None",
        "code" : "None"
    },
    {
        "name" : "Disabled Veterans",
        "code" : "Disabled Veterans"
    },
    {
        "name" : "Other Protected Veterans",
        "code" : "Other Protected Veterans"
    },
{
        "name" : "Armed Forces Service Medal Veterans",
        "code" : "Armed Forces Service Medal Veterans"
    },
    {
        "name" : "Recently Separated Veterans",
        "code" : "Recently Separated Veterans"
    },
    {
        "name" : "Choose Not to Disclose",
        "code" : "Choose Not to Disclose"
    }                
    ];     

}]);