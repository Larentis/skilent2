/***
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Candidate = mongoose.model('Candidate'),
    _ = require('underscore'),
    multiparty = require('multiparty'),
    knox = require('knox'),
    moment = require('moment-timezone'),    
    config = require('../../config/config');



/**
 * Find candidate by id
 */
exports.candidate = function(req, res, next, id) {
    Candidate.load(id, function(err, candidate) {
        if (err) return next(err);
        if (!candidate) return next(new Error('Failed to load candidate ' + id));
        req.candidate = candidate;
        next();
    });
};



/**
 * Create a candidate and save file to S3
 */
exports.create = function(req, res) {
    var candidate = new Candidate();
    candidate.user = req.user.id;
    candidate.updatedBy = req.user.id;


    var s3Client = knox.createClient({
      secure: false,
      key: config.s3.key,
      secret: config.s3.secret,
      bucket: config.s3.bucket
    });
    var headers = {
      'x-amz-acl': 'public-read',
    };

    var form = new multiparty.Form();

    form.on('field', function(name, value) {
        if (name === 'candidate') {
            // clone object using underscore util
            candidate = _.extend(candidate, JSON.parse(value));           
        }
    });

    form.on('part', function(part) {
        if (! part.filename) return;

        headers['Content-Length'] = part.byteCount;
        headers['Content-Type'] = part.headers['content-type'];
        var destPath = '/resumes/' + moment().format("MMDDYYYY-HHmmss") + "_" + part.filename;

       candidate.resume = {
            filename : part.filename,
            path : "https://" + config.s3.bucket + ".s3.amazonaws.com" + destPath,
            size : part.byteCount,
            contentType : part.headers['content-type']
        };

        s3Client.putStream(part, destPath, headers, function(s3Err, s3Response) {
            if (s3Err) res.send(400, "Error streaming file: " + s3Err.message);

            //res.statusCode = s3Response.statusCode;
            s3Response.resume();            
        });

    });


    form.on('error', function (err){
        console.log('Error inserting candidate');
        res.send(400, "Error inserting candidate: " + err.message);
        //throw err;
    });

    form.on('close', function (){
        console.log("My candidate: " + candidate); 
        candidate.save(function(saveErr) {
            if (saveErr) {
                return res.send(400, "Error saving candidate: " + saveErr);
            } else {
                res.json(candidate);
            }
        });

    });
    
    form.parse(req);
};


/**
 * Create a candidate when server allows writing to filesystem
 */
exports.create_old = function(req, res) {
    var candidate = new Candidate();
    candidate.user = req.user.id;
    candidate.updatedBy = req.user.id;
    var myUploadDir = config.root + '/public/uploads';

    //console.log("My header: " + req.get('Content-Type'));


    var form = new multiparty.Form({
        uploadDir : myUploadDir
    });


    form.on('error', function (err){
        console.log('Error streaming file');
        res.send(400, "Error streaming file: " + err.message);
        //throw err;
    });
    
    form.parse(req, function(err, fields, files) {
      if (err) {
        res.send(400, "Request error: " + err.message);
      }

      // create candidate object from field values
      candidate.name = {
        first : fields.nameFirst,
        last: fields.nameLast
      };
      candidate.title = fields.title;
      candidate.salary = {
        current: fields.salaryCurrent,
        desired: fields.salaryDesired
      };
      candidate.location = {
        city: fields.locationCity,
        state: fields.locationState,
        market: fields.locationMarket
      };
      candidate.description = fields.description;
      candidate.phone = fields.phone;
      candidate.email = fields.email;   
      if (files.file) {   
          candidate.resume = {
            filename : files.file[0].originalFilename,
            path : files.file[0].path,
            size : files.file[0].size,
            contentType : files.file[0].headers['content-type']
          };
      }
      if (fields.skills && fields.skills[0] !== "") {
        console.log("i am in skills");
        // For some reason FormData() array field comes back as a string, 
        // and it's assigned to the first element of the array
        // Split the string at position 0 and push each item to candidate.skills 
        var partsOfStr = fields.skills[0].split(',');
        candidate.skills = [];
        for (var i in partsOfStr) {
            candidate.skills.push(partsOfStr[i]);
        }        
      }

      candidate.save(function(err) {
        if (err) {
            return res.send(400, { errors: err });
        } else {
            res.jsonp(candidate);
        }
      });

    });
};



/**
 * Update a candidate
 */
exports.update = function(req, res) {
    var candidate = req.candidate;
    console.log("old candidate is: " + JSON.stringify(req.candidate));
    candidate = _.extend(candidate, req.body);
    candidate.updated = Date.now();
    candidate.updatedBy = req.user.id;
    console.log("updated candidate is: " + JSON.stringify(candidate));

    candidate.save(function(err) {
        if (err) {
            return res.send(400, { errors: err });
        } else {
            res.send(204);
        }
    });
};

/**
 * Update a candidate
 */
exports.updateNotes = function(req, res) {
    //var candidate = req.candidate;
    //candidate = _.extend(candidate, req.body);
    //candidate.updated = Date.now();
    //candidate.updatedBy = req.user.id;

    console.log('body is: ' + JSON.stringify(req.body));

    Candidate.findByIdAndUpdate(req.params.candidateId, 
        { $push: { notes: req.body }, $set : {updated: Date.now(), updatedBy : req.user.id}}, 
        function (err, myjob) {
            if (err) return res.send(400, { errors: err });
            res.send(204);
    });
};




/**
 * Delete an candidate
 */
exports.destroy = function(req, res) {
    var candidate = req.candidate;

    candidate.remove(function(err) {
        if (err) {
            res.send(400, { errors: err });
        } else {
            res.send(204);
        }
    });
};

/**
 * Show a candidate
 */
exports.show = function(req, res) {
    res.jsonp(req.candidate);
};

/**
 * List of Candidates
 */
exports.all = function(req, res) {
    Candidate.find().sort('-created').populate('user', 'name username').exec(function(err, candidates) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(candidates);
        }
    });
};
