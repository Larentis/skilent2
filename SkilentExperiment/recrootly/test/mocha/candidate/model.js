/**
 * Module dependencies.
 */
var should = require('should'),
    app = require('../../../server'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Candidate = mongoose.model('Candidate');

//Globals
var user;
var candidate;

//The tests
describe('<Unit Test>', function() {
    describe('Model Candidate:', function() {
        beforeEach(function(done) {
            user = new User({
                name: { 
                    first: 'First name',
                    last: 'Last name'
                },
                email: 'test@test.com',
                //username: 'user',
                password: 'password',
                provider: 'local',
                regStatus: 'new',
                updated: Date.now                
            });

            user.save(function(err) {
                candidate = new Candidate({
                    name: {
                        first: 'Candidate First Name',
                        last: 'Candidate Last Name'
                    },
                    title: 'Candidate Title',
                    description: 'Candidate Description',
                    salary: {
                        current: '100,000.00',
                        desired: '200,000.00'
                    },
                    skills: ['Java', 'Python', 'JavaScript'],
                    phone: '212-509-8326',
                    email: 'developer@gmail.com',
                    location: {
                        city: 'New York',
                        state: 'NY',
                        market: 'U.S.'
                    },
                    updated: Date.now,
                    user: user
                });
                done();
            });
        });

        describe('Method Save', function() {
            it('should be able to save without problems', function(done) {
                return candidate.save(function(err) {
                    should.not.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without title', function(done) {
                candidate.title = '';

                return candidate.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without first name', function(done) {
                candidate.name.first = '';

                return candidate.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without last name', function(done) {
                candidate.name.last = '';

                return candidate.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without current salary', function(done) {
                candidate.salary.current = '';

                return candidate.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without desired salary', function(done) {
                candidate.salary.desired = '';

                return candidate.save(function(err) {
                    should.exist(err);
                    done();
                });
            });

            it('should be able to show an error when try to save without email', function(done) {
                candidate.email = '';

                return candidate.save(function(err) {
                    should.exist(err);
                    done();
                });
            });            

        });

        afterEach(function(done) {
            Candidate.remove({});
            User.remove({});
            done();
        });
        after(function(done){
            Candidate.remove().exec();
            User.remove().exec();
            done();
        });
    });
});
