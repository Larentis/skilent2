/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    Schema = mongoose.Schema;


/**
 + * Role Schema
 + */
var RoleSchema = new Schema({
    name: {type: String},
    applications : {
        api: { type : String, default : "/api/v1/applications" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    candidates : {
        api: { type : String, default : "/api/v1/candidates" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    jobs : {
        api: { type : String, default : "/api/v1/jobs" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    roles : {
        api: { type : String, default : "/api/v1/roles" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    users : {
        api: { type : String, default : "/users" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    applicationind : {
        api: { type : String, default : "/api/v1/applications/:applicationId" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    candidateind : {
        api: { type : String, default : "/api/v1/candidates/:candidateId" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    jobind : {
        api: { type : String, default : "/api/v1/jobs/:jobId" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    roleind : {
        api: { type : String, default : "/api/v1/roles/:roleId" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    userind : {
        api: { type : String, default : "/users/:userId" },
        create: { type : Boolean, default : "false" },
        read: { type : Boolean, default : "false" },
        update: { type : Boolean, default : "false" },
        delete: { type : Boolean, default : "false" }
    },
    created: {
        type: Date,
        default: Date.now,
        required:true
    },
    updated : { type: Date, default: Date.now, required:true },
    updatedBy: {
        type: Schema.ObjectId,
        ref: 'User',
        required : true
    }
});

RoleSchema.path('name').index({ unique: true });
/**
 * Statics
 */
RoleSchema.statics.load = function(id, cb) {
    this.findRole({
        _id: id
    }).populate('updatedBy', 'name email roles').exec(cb);
};

mongoose.model('Role', RoleSchema);