var config = require('../../config/config'),
  nodemailer = require('nodemailer'),
  path = require('path'),
  templatesDir = path.resolve(__dirname, '..', 'views/mailer'),
  emailTemplates = require('email-templates');

var EmailAddressRequiredError = new Error('email address required');

// create a defaultTransport using gmail and authentication that are
// storeed in the `config.js` file.
// var smtpTransport = nodemailer.createTransport("SMTP", {
//   service: "Gmail",
//   auth: {
//     XOAuth2: {
//       user: "cmarvino@comtechllc.com", 
//       clientId: "938679608323-fdqr9kfmojkuv3rvko69av0l7r9r8bvl.apps.googleusercontent.com",
//       clientSecret: "0hO9sIBtER_TOEvIzQRJNb5W",
//       refreshToken: "1/tN0FKO4OZy4owwPqwv9tRNGJLGhUEp3qNSbh1i9p6WE"
//     }
//   }
// });

// var mailOptions = {
//   from: "csrx212@gmail.com",
//   to: "cmarvino@comtechllc.com",
//   subject: "Hello",
//   generateTextFromHTML: true,
//   html: "<b>Hello world</b>"
// };

// smtpTransport.sendMail(mailOptions, function(error, response) {
//   if (error) {
//     console.log("OG Error: " + error);
//   } else {
//     console.log("OG Response: " + response);
//   }
//   smtpTransport.close();
// });

var myTransport = nodemailer.createTransport("SMTP", {
  service: "Gmail",
  auth: {
    XOAuth2: {
    user: config.mailer.auth.user,
    clientId: config.mailer.auth.clientId,
    clientSecret: config.mailer.auth.clientSecret,
    refreshToken: config.mailer.auth.refreshToken
  }
 }
});


exports.sendOne = function (templateName, locals, fn) {
 // make sure that we have an user email
 if (!locals.email) {
   return fn(EmailAddressRequiredError);
 }
 // make sure that we have a message
 if (!locals.subject) {
   return fn(EmailAddressRequiredError);
 }
 emailTemplates(templatesDir, function (err, template) {
   if (err) {
     //console.log(err);
     return fn(err);
   }

   var toEmail;
   //set toEmail depending on the template
   if (templateName == 'job-application') toEmail = locals.toEmail;
   else if (templateName == 'registration') toEmail = locals.email;
   else toEmail = locals.userEmail;


   // Send a single email
   template(templateName, locals, function (err2, html, text) {
     if (err2) {
       //console.log(err);
       return fn(err2);
     }
     // if we are testing don't send out an email instead return
     // success and the html and txt strings for inspection
     // if (process.env.NODE_ENV === 'test') {
     //   return fn(null, '250 2.0.0 OK 1350452502 s5sm19782310obo.10', html, text);
     // }
     myTransport.sendMail({
       from: locals.email,
       to: toEmail,
       subject: locals.subject,
       html: html,
       //generateTextFromHTML: true,
       text: text
     }, function (err3, responseStatus) {
       if (err3) {
         return fn(err3);
       }
       return fn(null, responseStatus.message, html, text);
     });
   });
 });
};
