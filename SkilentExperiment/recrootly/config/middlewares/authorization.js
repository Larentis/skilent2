
/**
 * Generic require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

/**
 * User authorizations routing middleware
 */
exports.user = {
    hasAuthorization: function(req, res, next) {
       // if (req.profile.id != req.user.id) {
          //  return res.send(403, 'User is not authorized');
        //}
        next();
    }
};

/**
 * Candidate authorizations routing middleware
 */
exports.candidate = {
    hasAuthorization: function(req, res, next) {
        //candidates created from linkedin have no user object
        //if (req.candidate.user) {
          //  if (req.candidate.user.id != req.user.id) {
            //    return res.send(403, 'User is not authorized');
            //}
       // }
        next();
    }
};


/**
 * Job authorizations routing middleware
 */
exports.job = {
    hasAuthorization: function(req, res, next) {
        // is the signed-in user the same as the user associated with the job record?
        //if (req.job.user.id != req.user.id) {
           // return res.send(403, 'User is not authorized');
        //}
        next();
    }
};

