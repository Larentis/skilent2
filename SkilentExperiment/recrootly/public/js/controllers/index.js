angular.module('mean.system').controller('IndexController', ['$scope', 'Global', '$location', function ($scope, Global, $location) {
	$scope.global = Global;

	// if user is logged in and tries to access the index, redirect him/her to candidates page
	$scope.checkUser = function () {
		if ($scope.global.user) {
			$location.path('candidates');
		}
	};
}]);