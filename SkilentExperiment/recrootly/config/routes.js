module.exports = function(app, passport, auth) {

    // All routes set app.locals.mypath
    app.all('*', function(req, res, next){
        app.locals.mypath = "";
        next();
    });

    var roles = require('../app/controllers/roles');

    //User Routes
    var users = require('../app/controllers/users');
    app.get('/signin', users.signin);
    app.get('/register', users.register);
    app.get('/signout', users.signout);
    app.get('/signin/success', users.success);

    // Company Page Express Routes
    var co = require('../app/controllers/companypages');
    app.param('cjobid', co.load);
    app.get('/comtechllc/jobs', co.list);
    app.get('/comtechllc/jobs/:cjobid', co.show);
    app.get('/comtechllc/jobs/:cjobid/eeo', co.showEEO);
    app.post('/comtechllc/jobs/:cjobid/apply', co.showApply);

    //Job Application Routes for company pages
    var apps = require('../app/controllers/applications');
    //app.post('/api/v1/applications/linkedin', apps.createLinkedin);
    app.post('/comtechllc/applications', apps.create);

    // route to test if the user is logged in or not
    app.get('/loggedin', function(req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    });

    //Setting up the users api

    app.get('/api/v1/users', auth.requiresLogin, roles.isPermitted, users.all);
    app.get('/api/v1/users/new', auth.requiresLogin, users.allnew);
    app.get('/api/v1/users/:userId', auth.requiresLogin, roles.isPermitted, users.showUser);
    //app.post('/api/v1/users/:userId', auth.requiresLogin, users.updateProfile);
    app.get('/regconf', users.confirmation);
    app.post('/api/v1/users', roles.isPermitted, users.createI);
    app.post('/api/v1/users/internal', users.create);
    app.put('/users/:userId', users.updateProfile);
    app.del('/users/:userId', users.destroy);
    app.put('/api/v1/users/:id/regStatus/approve', auth.requiresLogin,  users.approve);
    app.put('/api/v1/users/:id/regStatus/reject', auth.requiresLogin,  users.reject);

    // app.post('/users/session', passport.authenticate('local', {
    //     failureRedirect: 'signin',
    //     failureFlash: true
    //     }),        
    //     users.session);

    app.post('/users/session', function(req, res, next) {
      passport.authenticate('local', {
         failureFlash: true
         }, 
      function(err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.render('users/signin', {message: info.message}); }
        switch (user.regStatus) {
            case "Approved":
                req.logIn(user, function(err) {
                  console.log("Logging user in...");
                  if (err) { return next(err); }
                  return res.redirect('/signin/success');
                });
                break;                
            case "New":
                return res.redirect('/regconf');
                break; 
            case "Rejected":
                return res.redirect('/signin');
                break; 
            default:
                return res.redirect('/regconf');
        }
      })(req, res, next);
    });

    app.get('/users/me', users.me);
    app.get('/users/:userId', users.show);

    //Setting the facebook oauth routes
    // app.get('/auth/facebook', passport.authenticate('facebook', {
    //     scope: ['email', 'user_about_me'],
    //     failureRedirect: '/signin'
    // }), users.signin);

    // app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    //     failureRedirect: '/signin'
    // }), users.authCallback);

    //Setting the github oauth routes
    // app.get('/auth/github', passport.authenticate('github', {
    //     failureRedirect: '/signin'
    // }), users.signin);

    // app.get('/auth/github/callback', passport.authenticate('github', {
    //     failureRedirect: '/signin'
    // }), users.authCallback);

    //Setting the twitter oauth routes
    // app.get('/auth/twitter', passport.authenticate('twitter', {
    //     failureRedirect: '/signin'
    // }), users.signin);

    // app.get('/auth/twitter/callback', passport.authenticate('twitter', {
    //     failureRedirect: '/signin'
    // }), users.authCallback);


    // app.get('/auth/twitter/callback', function(req, res, next) {
    //   passport.authenticate('twitter', {
    //      failureFlash: true
    //      }, 
    //   function(err, user, info) {
    //     if (err) { return next(err); }
    //     if (!user) { return res.render('users/signin', {message: info.message}); }
    //     switch (user.regStatus) {
    //         case "Approved":
    //             req.logIn(user, function(err) {
    //               console.log("Logging user in...");
    //               if (err) { return next(err); }
    //               return res.redirect('/signin/success');
    //             });
    //             break;                
    //         case "New":
    //             return res.redirect('/regconf');
    //             break; 
    //         case "Rejected":
    //             return res.redirect('/signin');
    //             break; 
    //         default:
    //             return res.redirect('/regconf');
    //     }
    //   })(req, res, next);
    // });    

    //Setting the google oauth routes
    // app.get('/auth/google', passport.authenticate('google', {
    //     failureRedirect: '/signin',
    //     scope: [
    //         'https://www.googleapis.com/auth/userinfo.profile',
    //         'https://www.googleapis.com/auth/userinfo.email'
    //     ]
    // }), users.signin);

    // app.get('/auth/google/callback', passport.authenticate('google', {
    //     failureRedirect: '/signin'
    // }), users.authCallback);

    // linkedin routes
    app.get('/auth/linkedin', passport.authenticate('linkedin', {
        failureRedirect: '/signin',
        scope: [ 'r_emailaddress' ]
    }), users.signin);

    // app.get('/auth/linkedin/callback', passport.authenticate('linkedin', {
    //     failureRedirect: '/signin'
    // }), users.authCallback);

    app.get('/auth/linkedin/callback', function(req, res, next) {
      passport.authenticate('linkedin', {
         failureFlash: true
         }, 
      function(err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.render('users/signin', {message: info.message}); }
        switch (user.regStatus) {
            case "Approved":
                req.logIn(user, function(err) {
                  console.log("Logging LinkedIn user in...");
                  if (err) { return next(err); }
                  return res.redirect('/signin/success');
                });
                break;                
            case "New":
                return res.redirect('/regconf');
                break; 
            case "Rejected":
                return res.redirect('/signin');
                break; 
            default:
                return res.redirect('/regconf');
        }
      })(req, res, next);
    }); 

    //Finish with setting up the userId param
    app.param('userId', users.user);

    //Candidate Routes
    var candidates = require('../app/controllers/candidates');
    app.get('/api/v1/candidates', auth.requiresLogin, roles.isPermitted, candidates.all);
    app.post('/api/v1/candidates', auth.requiresLogin, roles.isPermitted, candidates.create);
    app.get('/api/v1/candidates/:candidateId', auth.requiresLogin, roles.isPermitted, candidates.show);
    app.put('/api/v1/candidates/:candidateId', auth.requiresLogin, roles.isPermitted, candidates.update);
    app.put('/api/v1/candidates/:candidateId/notes', auth.requiresLogin, roles.isPermitted, candidates.updateNotes);
    app.del('/api/v1/candidates/:candidateId', auth.requiresLogin, auth.candidate.hasAuthorization, roles.isPermitted,candidates.destroy);
    //Finish with setting up the candidateId param
    app.param('candidateId', candidates.candidate);

    //Role Routes

    app.param('roleId' , roles.role);
    app.get('/api/v1/roles', auth.requiresLogin, roles.isPermitted, roles.all);
    app.post('/api/v1/roles', auth.requiresLogin, roles.isPermitted, roles.create);
    app.put('/api/v1/roles', auth.requiresLogin, roles.isPermitted, roles.update);
    app.get('/api/v1/roles/:roleId', auth.requiresLogin, roles.isPermitted, roles.showRole);
    app.put('/api/v1/roles/:roleId', auth.requiresLogin, roles.isPermitted, roles.update);

    //Job Routes
    var jobs = require('../app/controllers/jobs');
    app.get('/api/v1/jobs', auth.requiresLogin, roles.isPermitted, jobs.all);
    app.param('jobId', jobs.job);    
    app.post('/api/v1/jobs', auth.requiresLogin, roles.isPermitted, jobs.create);
    app.get('/api/v1/jobs/:jobId', auth.requiresLogin, roles.isPermitted, jobs.show);
    app.put('/api/v1/jobs/:jobId', auth.requiresLogin, roles.isPermitted, jobs.update);
    app.del('/api/v1/jobs/:jobId', auth.requiresLogin, auth.job.hasAuthorization, roles.isPermitted, jobs.destroy);
    app.get('/api/v1/jobs/open',
      // Authenticate using either HTTP Basic or Digest credentials, with session support disabled.
      passport.authenticate('basic', { session: false }),
      jobs.allopen
    );        

    //Job Application Routes API 
    app.param('applicationId', apps.application);     
    app.get('/api/v1/applications', auth.requiresLogin, roles.isPermitted, apps.all);
    app.get('/api/v1/applications/:applicationId', auth.requiresLogin, roles.isPermitted, apps.show);
    app.put('/api/v1/applications/:applicationId', auth.requiresLogin, roles.isPermitted, apps.update);

    app.get('/auth/lapply?', function(req, res, next) {
        req.session.passport = req.query;
        passport.authenticate('applywithlinkedin', {
            failureRedirect: '/comtechllc/jobs',
            scope: ['r_fullprofile', 'r_emailaddress', 'r_basicprofile', 'r_contactinfo'],
            passReqToCallback: true
        })(req, res, next);
    });

    app.get('/auth/lapply/callback', function(req, res, next) {

        passport.authenticate('applywithlinkedin', {
                failureFlash: true,
                passReqToCallback: true
            },function(err, cand, info) {

                if (err) { return next(err); }
                return res.render('comtechllc/success');

            }
        )(req, res, next);
    });

    // Social Search
    var ss = require('../app/controllers/socialsearch');
    app.get('/api/v1/twSearch', auth.requiresLogin, ss.twittersearch);
    app.get('/api/v1/liSearch', auth.requiresLogin, ss.lisearch);
    app.get('/api/v1/seSearch/relatedTags', auth.requiresLogin, ss.seTags);
    app.get('/api/v1/sfSearch/relatedTags', auth.requiresLogin, ss.sfTags);
    app.get('/api/v1/seSearch/topTagAnswerers', auth.requiresLogin, ss.seTopAnswerers);
    app.get('/api/v1/ghSearch', auth.requiresLogin, ss.ghsearch);

    // Amazon s3 parameters
    var amazon = require('../app/controllers/amazon'); 
    app.get('/api/v1/s3Options', auth.requiresLogin, amazon.s3Options);


    //Home route
    var index = require('../app/controllers/index');
    app.get('/', index.render);

    // redirect all others to the index (HTML5 history)
    //app.get('*', index.render);

};
