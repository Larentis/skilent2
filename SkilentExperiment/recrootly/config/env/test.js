module.exports = {
    db: "mongodb://localhost/recrootly-test",
    port: 3001,
    redisDb : {
      "host": "localhost",
      "port": 6379,
      "password": null,
      "namespace": "recrootly-test"
    },
    app: {
        name: "skilent - Candidate Tracking System"
    },
    s3: {
        key: "AKIAJ2DHLQGY3E3GYCKQ",
        secret: "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",
        bucket: "skilent-dev"
    },
    stackexchange: {
        path: "https://api.stackexchange.com/2.1/",        
        key: "",
        secret: "ICjmnj5et4lkXEJw9MxX4Q(("
    },        
    linkedin: {
        clientID: "API_KEY",
        clientSecret: "SECRET_KEY",
        callbackURL: "http://localhost:3000/auth/linkedin/callback"
    },     
    facebook: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/facebook/callback"
    },
    twitter: {
        clientID: "CONSUMER_KEY",
        clientSecret: "CONSUMER_SECRET",
        callbackURL: "http://localhost:3000/auth/twitter/callback"
    },
    github: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/github/callback"
    },
    google: {
        clientID: "APP_ID",
        clientSecret: "APP_SECRET",
        callbackURL: "http://localhost:3000/auth/google/callback"
    }
}