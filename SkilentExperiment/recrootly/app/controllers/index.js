/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('underscore');


exports.render = function(req, res) {
    if (req.user) {
        res.locals.mypath = '';
        //res.render('/#!/candidates/');
    }
    else
	{
        res.locals.mypath = 'hp';
    }	
    res.render('index', {
        user: req.user ? JSON.stringify(req.user) : "null"
    });
};
