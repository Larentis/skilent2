//Jobs service used for jobs REST endpoint
// angular.module('mean.jobs').factory("JobsSrv", ['$resource', function($resource) {
//     return $resource('/api/v1/jobs/:jobId', {
//         jobId: '@_id'
//     }, {
//         update: {
//             method: 'PUT'
//         }
//     });
// }]);


//Jobs factory service used for REST endpoint
angular.module('mean.jobs').factory('JobsSrv', ['$http', '$q', function($http, $q) {

	var urlBase = '/api/v1/jobs';
	var Jobs = {};

	Jobs.getJob = function(id) {
		return $http.get(urlBase + '/' + id);
	};

	Jobs.getAll = function() {
		return $http.get(urlBase);
	};

	Jobs.insertJob = function(job) {
		return $http.post(urlBase, job);
	};

	Jobs.updateJob = function(job) {
		return $http.put(urlBase + '/' + job._id, job);
	};

	Jobs.remove = function(job) {
		return $http.delete(urlBase + '/' + job._id, job);
	};

	return Jobs;

}]);

