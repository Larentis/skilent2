/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	User = mongoose.model('User'),
	config = require('../../config/config'),
	mailer = require('./mailer'),
	Job = mongoose.model('Job');



/**
 * Show job list for company page
 */
exports.list = function(req, res) {
	Job.find({ 'status': 'Open - Funded', 'company' : 'Comtech' }).sort('-created').populate('user', 'name email').populate('leadRec', 'name').exec(function(err, jobs) {
		if (err) {
			return res.render('500');
		} else {
			res.render('comtechllc/listings', {
				title: 'Job Listings',
				jobs: jobs,
				message: req.flash('error'),
			});            
		}
	});
};

/**
 * Load job id
 */
exports.load = function(req, res, next, id) {
	Job.load(id, function(err, job) {
		if (err) return next(err);
		if (!job) return next(new Error('Job ' + id + ' not found'));
		req.cjob = job;
		next();
	});
};


/**
 * View job by id
 */
exports.show = function(req, res) {
	res.render('comtechllc/show', {
	title: req.cjob.title,
	job: req.cjob
	});
};

/**
 * Show EEO by job id
 */
exports.showEEO = function(req, res) {	
	res.render('comtechllc/eeo', {
		messages: req.flash('sourceMissing'),
		title: req.cjob.title,
		job: req.cjob
	});
};

/**
 * Show application by job id
 */
exports.showApply = function(req, res) {


	if (req.body.jobSource) {

		// create eeo Metat data for LinkedIn
		var eeoMeta = [];
		eeoMeta.push(req.body.gender);
		eeoMeta.push(req.body.race);
		eeoMeta.push(req.body.veteran);

		if (req.body.jobSource == "Other") {
			// If users selects "Other", they must specify the source
			if (req.body.otherSource === "" ) {
				// if user does not specify "Other" source, re-render the view with error
				req.flash('sourceMissing', 'Please specify a source. Tell us how you heard about us.');
				res.redirect('comtechllc/jobs/'+ req.params.cjobid +'/eeo');
			}
			else {
				eeoMeta.push(req.body.otherSource);

				// User specified "other" source
				res.render('comtechllc/apply', {
					title: req.cjob.title,
					job: req.cjob,
					meta: eeoMeta.join(','),
					jobSource: req.body.otherSource
				});
			}
		} else {
			eeoMeta.push(req.body.jobSource);

			// user selected a proper job source
			res.render('comtechllc/apply', {
				title: req.cjob.title,
				job: req.cjob,
				meta: eeoMeta.join(','),
				jobSource: req.body.jobSource
			});
		}
	}
	else {
		req.flash('sourceMissing', 'Please specify a source. Tell us how you heard about us.');
		res.redirect('comtechllc/jobs/'+ req.params.cjobid +'/eeo');
	}

};

