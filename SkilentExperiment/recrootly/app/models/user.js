/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    crypto = require('crypto'),
    _ = require('underscore'),
    authTypes = ['github', 'twitter', 'facebook', 'google', 'linkedin'];


/**
 * User Schema
 */
var UserSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    name: {
        first: { type: String, required: true },
        last: { type: String }
    },
    phone: { type: String},
    email: { type: String, required: true },
    location: {
        city: { type: String },
        state: { type: String },
        market: { type: String, default: 'U.S.' }
    },
    notes: [{
        createdBy: {type: String},
        created: {type : Date},
        noteText: {type: String}
    }],
    provider: String,
    hashed_password: String,
    salt: String,
    facebook: {},
    twitter: {},
    github: {},
    google: {},
    linkedin: {},
    regStatus: { type: String, required: true },
    /*roles: [ {
        type: Schema.ObjectId,
        ref: 'Role'
    }] ,*/
    roles: [{ type: String }],
    updated : { type: Date, default: Date.now, required:true }
});



/**
 * Indexes
 */
UserSchema.path('email').index({ unique: true });
//UserScnode

/**
 * Virtuals
 */
UserSchema.virtual('password').set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
}).get(function() {
    return this._password;
});

UserSchema.virtual('fullname').get(function () {
    return this.name.first + ' ' + this.name.last;
});

/**
 * Validations
 */
var validatePresenceOf = function(value) {
    return value && value.length;
};

// the below 4 validations only apply if you are signing up traditionally
UserSchema.path('name.first').validate(function(firstname) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return firstname.length;
}, 'First name cannot be blank');

UserSchema.path('name.last').validate(function(lastname) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return lastname.length;
}, 'Last name cannot be blank');

UserSchema.path('email').validate(function(email) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return email.length;
}, 'Email cannot be blank');

// UserSchema.path('username').validate(function(username) {
//     // if you are authenticating by any of the oauth strategies, don't validate
//     if (authTypes.indexOf(this.provider) !== -1) return true;
//     return username.length;
// }, 'Username cannot be blank');

UserSchema.path('hashed_password').validate(function(hashed_password) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true;
    return hashed_password.length;
}, 'Password cannot be blank');


/**
 * Pre-save hook
 */
UserSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    if (!validatePresenceOf(this.password) && authTypes.indexOf(this.provider) === -1)
        next(new Error('Invalid password'));
    else
        next();
});

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */
    makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */
    encryptPassword: function(password) {
        if (!password || !this.salt) return '';
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
    }
};

/**
 * Statics
 */
UserSchema.statics.load = function(roleName, cb) {
    this.findOne({
        name: roleName
    }).exec(cb);
};

mongoose.model('User', UserSchema);