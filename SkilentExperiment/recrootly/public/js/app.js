angular.module('mean', ['ngCookies', 'ngResource', 'ngRoute', 'ngAnimate', 'ngStorage', 'ui.route', 'ui.bootstrap', 'mean.system', 'mean.candidates', 'mean.jobs', 'mean.applications', 'mean.users', 'mean.socialsearch', 'underscore', 'geolocation', 'leaflet-directive', 'angulartics', 'angulartics.google.analytics', 'ajoslin.promise-tracker', 'cgBusy', 'mean.roles', 'angularFileUpload']);

angular.module('mean.system', []);
angular.module('mean.candidates', []);
angular.module('mean.jobs', []);
angular.module('mean.applications', []);
angular.module('mean.users', []);
angular.module('mean.socialsearch', []);
angular.module('underscore', []);
angular.module('geolocation', []);
angular.module('leaflet-directive', []);
angular.module('angulartics', []);
angular.module('angulartics.google.analytics', []);
angular.module('ajoslin.promise-tracker', []);
angular.module('cgBusy', []);
angular.module('mean.roles', []);

