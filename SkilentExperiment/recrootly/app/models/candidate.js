/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    Schema = mongoose.Schema;


/**
 * Candidate Schema
 */
var CandidateSchema = new Schema({
    mainStatus: { type: String, default: 'NEW'},
    status: { type: String, default: 'Applied'},
    rejReason: { type: String},
    logDate: { type: Date},
    offStatus: { type: String },
    created: {
        type: Date,
        default: Date.now
    },
    updated : { type: Date, default: Date.now, required: true },
    updatedBy: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    name: {
        first: { type: String, required: true },
        last: { type: String, required: true }
    },    
    title: {
        type: String,
        default: '',
        trim: true
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    salary: {
        current: { type: String },
        desired: { type: String }
    },
    phone: { type: String },
    email: { type: String, required: true },
    location: {
        city: { type: String },
        zip: { type: String },
        state: { type: String },
        market: { type: String, default: 'U.S.' },
        country: { type: String }
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    candSource: { type: String, default: 'Company Website' },
    candSourceDetails: { type: String},
    skills: [{ type : String, index: true }],
    workAuth : { type: String, default: 'Unknown'},
    notes: [{
        createdBy: {type: String},
        created: {type : Date},
        noteText: {type: String}       
    }],
    resume : {
        filename : { type: String },
        path : { type: String },
        size : { type: Number },
        contentType : { type: String }
    },
    recommendations: [{
        recType: {type: String},
        recText: {type: String},
        recommender: {type : String}
    }],
    linkedinProfile: {type: String},
    highEducation : { type: String},
    highEducationOther : { type: String },
    positions: [{
        title: {type: String},        
        summary: {type: String},
        startDate: {
            month: {type: Number},
            year: {type: Number}
        },
        endDate: {
            month: {type: Number},
            year: {type: Number}
        },
        company: {type: String},
        isCurrent: {type: Boolean}
    }],
    languages: [{type: String}],
    certifications: [{type: String}],
    educations: [{
        fieldOfStudy: {type: String},
        degree: {type: String},
        schoolName: {type: String},
        activities: {type: String},
        startDate: {
            month: {type: Number},
            year: {type: Number}
        },
        endDate: {
            month: {type: Number},
            year: {type: Number}
        }        
    }],
    jobsAppliedTo: [ {
        type: Schema.ObjectId,
        ref: 'Application'
    }] 
});

/**
 * Validations
 */
//CandidateSchema.path('title').validate(function(title) {
//    return title.length;
//}, 'Title cannot be blank');



/**
 * Statics
 */
CandidateSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('user', 'name email').populate('updatedBy', 'name email')
        .populate('jobsAppliedTo', '_id job jobTitle status created').exec(cb);
};

CandidateSchema.set('toJSON', { virtuals: true });
CandidateSchema.set('toObject', { virtuals: true });
mongoose.model('Candidate', CandidateSchema);